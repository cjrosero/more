

/*call modal*/
$(document).on('click','.add-video',function(e){
	e.preventDefault();
	$('#modal-add-video').modal('show');
});
/*submit*/
$(document).on('click','.save-content',function(e){
	e.preventDefault();
	var title = $("#video-title").val();

	if (title.length > 0) {
		$.ajax({
			url: baseUrl+'video/add',
			type: 'POST',
			dataType: 'json',
			data: {title:title},
			success:function(response){

			},
			error:function(response){

			}
		});
	}
	
});

$(document).on('click','.set-active',function(e){
	e.preventDefault();
	var ele = $(this);
	var row_id = ele.attr('data-id');
	bootbox.confirm({
		title: "Set as active",
		message: "Are you sure you want to set this content?",
		size : "small",
		buttons: {
			confirm: {
				label: 'Yes',
				className: 'btn-success'
			},
			cancel: {
				label: 'No',
				className: 'btn-danger'
			}
		},
		callback: function (result) {
			if (result) {
				$.ajax({
					url: base_url + 'video/active',
					type: 'POST',
					dataType: 'json',
					data: {id: row_id},
					success:function(response){
						if (response.success) {
							new PNotify({
								delay: 5000,
								hide: true,
								title: 'Success',
								text: response.msg,
								type: 'success'
							}); 
						}
					},
					error:function(response){

					}
				});
			}
		}
	});

});

// function ValidURL(str) {
//   var pattern = new RegExp('^(https?:\/\/)?'+ // protocol
//     '((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|'+ // domain name
//     '((\d{1,3}\.){3}\d{1,3}))'+ // OR ip (v4) address
//     '(\:\d+)?(\/[-a-z\d%_.~+]*)*'+ // port and path
//     '(\?[;&a-z\d%_.~+=-]*)?'+ // query string
//     '(\#[-a-z\d_]*)?$','i'); // fragment locater
//   if(!pattern.test(str)) {
    
//     return false;
//   } else {
//     return true;
//   }
// }