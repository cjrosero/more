$(document).on('click','.add-member',function(e){
	$('#modal-add-member').modal('show');
});

$(document).on('click','.mem_update',function(e){
	var mem_id = $(this).siblings("span.hidden").html();
	var member_name = $(this).parent("div").siblings("div.details").children("label.member_name_details").html();
	var member_desc = $(this).parent("div").siblings("div.details").children("p.member_desc_details").html();
	$("#update_member_name").val(member_name);
	$("#update_member_desc").val(member_desc);
	$("#update_member_id").val(mem_id);
	$('#modal-update-member').modal('show');
});

$(document).on("click",".mem_delete",function(){
	var mem_id = $(this).siblings("span.hidden").html();

		bootbox.confirm({
			title: "Remove Member",
			message: "Are you sure you want to remove this member?",
			size : "small",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if (result) {
					$.ajax({
						url: base_url + 'team/remove',
						type: 'POST',
						dataType: 'json',
						data: {mem_id: mem_id},
						success:function(response){
							if (response.success) {
								new PNotify({
									delay: 5000,
									hide: true,
									title: 'Success',
									text: response.msg,
									type: 'success'
								}); 
							}
							setTimeout(function(){ location.reload(); }, 1500);
						},
						error:function(response){

						}
					});
				}
			}
		});
	
})
