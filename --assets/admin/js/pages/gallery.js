//INITIALIZE DROPZONES
Dropzone.autoDiscover = false;
var myDropzone = new Dropzone('#form-dropzone-material',{
  // The configuration we've talked about above
    url:baseUrl+'gallery/upload',
    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 5,
    maxFiles: 5,
    acceptedFiles: '.jpg, .jpeg, .png, .gif',
    maxFilesize: 500,
    addRemoveLinks:true, 

    // The setting up of the dropzone
    init: function() {
      myDropzone = this;
      
      myDropzone.on("maxfilesexceeded", function(file) {
        myDropzone.addFile(file);
        myDropzone.removeAllFiles();
        
      });
       myDropzone.on('queuecomplete', function(){
        setTimeout(function(){
          myDropzone.removeAllFiles();
        },3000);
      })      
    },
    success: function(file, response){
                setTimeout(function(){ location.reload(); }, 2500);
            }

});

$(document).on('click','.add-image',function(e){
	e.preventDefault();
	$('#modal-add-image').modal('show');
});

function uploadImage()
{
  myDropzone.options.url = baseUrl + 'gallery/upload/';

  if (myDropzone.getQueuedFiles().length > 0)
  {
    myDropzone.processQueue();
  }
  else
  {
    myDropzone.uploadFiles([]);
  }
  myDropzone.removeAllFiles();
}

$(document).on("click",".save-image",function(){
	uploadImage();
})


$("#select_all").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});


$(document).on("click","#remove-image",function(){
	var arr = [];

	$('.remove_images').each(function(){
		if($(this).is( ":checked" )){
			arr.push($(this).val());
		}
		
	})
	console.log(arr);
	if (arr.length == 0) {
		new PNotify({
				delay: 1500,
				hide: true,
				title: 'Notice!',
				text: 'Select Images To Be Deleted',
				type: 'notice'
			});
	}else{

		bootbox.confirm({
			title: "Remove Image",
			message: "Are you sure you want to remove images?",
			size : "small",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if (result) {
					$.ajax({
						url: base_url + 'gallery/remove',
						type: 'POST',
						dataType: 'json',
						data: {image_id: arr},
						success:function(response){
							if (response.success) {
								new PNotify({
									delay: 5000,
									hide: true,
									title: 'Success',
									text: response.msg,
									type: 'success'
								}); 
							}
							setTimeout(function(){ location.reload(); }, 1500);
						},
						error:function(response){

						}
					});
				}
			}
		});
	}
})
