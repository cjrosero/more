/*document ready*/
var content_editor;
$(document).ready(function(){
	content_editor = new nicEditor({fullPanel : true,iconsPath:baseUrl+'assets/admin/js/nicedit/nicEditorIcons.gif'}).panelInstance('why-content');
});
/*call modal*/
$(document).on('click','.add-why',function(e){
	e.preventDefault();
	$('#modal-add-why').modal('show');
});
/*submit*/
$(document).on('click','.save-content',function(e){
	e.preventDefault();
	var title = $("#why-title").val();
	var content = content_editor.instanceById('why-content').getContent();
	if (content.length > 0 && title.length > 0) {
		$.ajax({
			url: baseUrl+'why/add',
			type: 'POST',
			dataType: 'json',
			data: {content:content,title:title},
			success:function(response){

			},
			error:function(response){

			}
		});
	}
	
});

$(document).on('click','.set-active',function(e){
	e.preventDefault();
	var ele = $(this);
	var row_id = ele.attr('data-id');
	bootbox.confirm({
		title: "Set as active",
		message: "Are you sure you want to set this content?",
		size : "small",
		buttons: {
			confirm: {
				label: 'Yes',
				className: 'btn-success'
			},
			cancel: {
				label: 'No',
				className: 'btn-danger'
			}
		},
		callback: function (result) {
			if (result) {
				$.ajax({
					url: base_url + 'why/active',
					type: 'POST',
					dataType: 'json',
					data: {id: row_id},
					success:function(response){
						if (response.success) {
							new PNotify({
								delay: 5000,
								hide: true,
								title: 'Success',
								text: response.msg,
								type: 'success'
							}); 
						}
					},
					error:function(response){

					}
				});
			}
		}
	});

});