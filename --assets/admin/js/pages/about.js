/*document ready*/
var content_editor;
$(document).ready(function(){
	content_editor = new nicEditor({fullPanel : true,iconsPath:baseUrl+'assets/admin/js/nicedit/nicEditorIcons.gif'}).panelInstance('about-content');
});
/*call modal*/
$(document).on('click','.add-about',function(e){
	e.preventDefault();
	$('#modal-add-about').modal('show');
});
/*submit*/
$(document).on('click','.save-content',function(e){
	e.preventDefault();
	var content = content_editor.instanceById('about-content').getContent();
	if (content.length > 0) {
		$.ajax({
			url: baseUrl+'about/add',
			type: 'POST',
			dataType: 'json',
			data: {content:content},
			success:function(response){

			},
			error:function(response){

			}
		});
	}
	
});

$(document).on('click','.set-active',function(e){
	e.preventDefault();
	var ele = $(this);
	var row_id = ele.attr('data-id');
	bootbox.confirm({
		title: "Set as active",
		message: "Are you sure you want to set this content?",
		size : "small",
		buttons: {
			confirm: {
				label: 'Yes',
				className: 'btn-success'
			},
			cancel: {
				label: 'No',
				className: 'btn-danger'
			}
		},
		callback: function (result) {
			if (result) {
				$.ajax({
					url: base_url + 'about/active',
					type: 'POST',
					dataType: 'json',
					data: {id: row_id},
					success:function(response){
						if (response.success) {
							new PNotify({
								delay: 5000,
								hide: true,
								title: 'Success',
								text: response.msg,
								type: 'success'
							}); 
						}
					},
					error:function(response){

					}
				});
			}
		}
	});

});