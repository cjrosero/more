var content_editor;
$(document).ready(function(){
	content_editor = new nicEditor({fullPanel : true,iconsPath:baseUrl+'assets/admin/js/nicedit/nicEditorIcons.gif'}).panelInstance('about-content');
	content_e_editor = new nicEditor({fullPanel : true,iconsPath:baseUrl+'assets/admin/js/nicedit/nicEditorIcons.gif'}).panelInstance('e-about-content');
});

$(document).on('click','.add-services',function(e){
	e.preventDefault();
	$('#modal-add-services').modal('show');
});

/*submit*/
$(document).on('click','.save-content',function(e){
	e.preventDefault();
	var content = content_editor.instanceById('about-content').getContent();
	var title = $('#s-title').val();
	if (content.length > 0) {
		$.ajax({
			url: baseUrl+'services/add',
			type: 'POST',
			dataType: 'json',
			data: {
				content:content,
				title:title
			},
			success:function(response){
				if(response.status){
					location.reload();
				}else{
					alert();
				}
				
			},
			error:function(response){

			}
		});
	}

});

$(document).on("click","#edit_services",function(){
	var service_id = $(this).siblings("span.hidden").html();
	$("#service_id").val(service_id);

	var title = $(this).closest("tr").children("td.serv_title").html();
	var description = $(this).closest("tr").children("td.serv_desc").html();

	$("#e-title").val(title);
	content_e_editor.instanceById('e-about-content').setContent(description);

	$("#modal-edit-services").modal("show");
})

//save edit
$(document).on('click','.update-content',function(e){
	e.preventDefault();
	var service_id = $("#service_id").val();
	var content = content_e_editor.instanceById('e-about-content').getContent();
	var title = $('#e-title').val();
	if (content.length > 0) {
		$.ajax({
			url: baseUrl+'services/edit',
			type: 'POST',
			dataType: 'json',
			data: {
				content:content,
				title:title,
				service_id:service_id
			},
			success:function(response){
				if(response.status){
					location.reload();
				}else{
					new PNotify({
							delay: 5000,
							hide: true,
							title: 'Error',
							text: "Something Went Wrong.",
							type: 'error'
						}); 
				}
				
			},
			error:function(response){

			}
		});
	}

});


//delete service
$(document).on("click","#remove_services",function(){
	var service_id = $(this).siblings("span.hidden").html();
	$("#service_id").val(service_id);

		bootbox.confirm({
		title: "Remove Service",
		message: "Are you sure you want to remove this service?",
		size : "small",
		buttons: {
			confirm: {
				label: 'Yes',
				className: 'btn-success'
			},
			cancel: {
				label: 'No',
				className: 'btn-danger'
			}
		},
		callback: function (result) {
			if (result) {
				$.ajax({
					url: base_url + 'services/remove',
					type: 'POST',
					dataType: 'json',
					data: {service_id: service_id},
					success:function(response){
						if (response.success) {
							new PNotify({
								delay: 5000,
								hide: true,
								title: 'Success',
								text: response.msg,
								type: 'success'
							}); 
						}
						setTimeout(function(){ location.reload(); }, 1500);
					},
					error:function(response){

					}
				});
			}
		}
	});

})