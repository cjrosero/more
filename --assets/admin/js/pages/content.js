var content_editor;
$(document).ready(function(){
	content_editor = new nicEditor({fullPanel : true,iconsPath:baseUrl+'assets/admin/js/nicedit/nicEditorIcons.gif'}).panelInstance('content');
	content_e_editor = new nicEditor({fullPanel : true,iconsPath:baseUrl+'assets/admin/js/nicedit/nicEditorIcons.gif'}).panelInstance('e-content');
	$(".selectpicker").selectpicker();

});

$(document).on('click','.add-contents',function(e){
	e.preventDefault();
	$('#modal-add-contents').modal('show');
});

/*submit*/
$(document).on('click','.save-content',function(e){
	e.preventDefault();
	var content = content_editor.instanceById('content').getContent();
	var title = $('#title').val();
	var type = $('#nav_type').val();

	if (content.length > 0) {
		$.ajax({
			url: baseUrl+'Dashboard/add',
			type: 'POST',
			dataType: 'json',
			data: {
				content:content,
				title:title,
				type:type
			},
			success:function(response){
				if(response.status){
					location.reload();
				}else{
					alert();
				}
				
			},
			error:function(response){

			}
		});
	}

});

$(document).on("click","#edit_contents",function(){
	var content_id = $(this).siblings("span.hidden").html();
	$("#content_id").val(content_id);

	var title = $(this).closest("tr").children("td.cont_title").html();
	var description = $(this).closest("tr").children("td.cont_desc").html();
	var type = $(this).closest("tr").children("td.cont_type").html();
	$('#e-nav_type').val(type);
	$('#e-nav_type').selectpicker('refresh')


	$("#e-title").val(title);
	content_e_editor.instanceById('e-content').setContent(description);

	$("#modal-edit-contents").modal("show");
})

//save edit
$(document).on('click','.update-content',function(e){
	e.preventDefault();
	var content_id = $("#content_id").val();
	var content = content_e_editor.instanceById('e-content').getContent();
	var title = $('#e-title').val();
	var type = $('#e-nav_type').val();
	
	if (content.length > 0) {
		$.ajax({
			url: baseUrl+'Dashboard/edit',
			type: 'POST',
			dataType: 'json',
			data: {
				content:content,
				title:title,
				content_id:content_id,
				type:type
			},
			success:function(response){
				if(response.status){
					location.reload();
				}else{
					new PNotify({
							delay: 5000,
							hide: true,
							title: 'Error',
							text: "Something Went Wrong.",
							type: 'error'
						}); 
				}
				
			},
			error:function(response){

			}
		});
	}

});


//delete service
$(document).on("click","#remove_contents",function(){
	var content_id = $(this).siblings("span.hidden").html();
	$("#content_id").val(content_id);

		bootbox.confirm({
		title: "Remove Service",
		message: "Are you sure you want to remove this service?",
		size : "small",
		buttons: {
			confirm: {
				label: 'Yes',
				className: 'btn-success'
			},
			cancel: {
				label: 'No',
				className: 'btn-danger'
			}
		},
		callback: function (result) {
			if (result) {
				$.ajax({
					url: base_url + 'Dashboard/remove',
					type: 'POST',
					dataType: 'json',
					data: {content_id: content_id},
					success:function(response){
						if (response.success) {
							new PNotify({
								delay: 5000,
								hide: true,
								title: 'Success',
								text: response.msg,
								type: 'success'
							}); 
						}
						setTimeout(function(){ location.reload(); }, 1500);
					},
					error:function(response){

					}
				});
			}
		}
	});

})