/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.13-MariaDB : Database - my_cms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `cms_banner` */

DROP TABLE IF EXISTS `cms_banner`;

CREATE TABLE `cms_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `banner_title` varchar(255) NOT NULL,
  `banner_description` varchar(255) NOT NULL,
  `banner_path` varchar(255) NOT NULL,
  `banner_status` varchar(255) DEFAULT NULL,
  `banner_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `cms_banner` */

insert  into `cms_banner`(`id`,`post_id`,`banner_title`,`banner_description`,`banner_path`,`banner_status`,`banner_date`) values (1,0,'Home Page','','b7dbe8f2107f877e02727f7113035f75.jpeg','publish','2017-09-30 23:42:09'),(2,0,'Moorestepen','','4850c3617ae69c678987029c42efeb38.jpeg','publish','2017-09-30 23:42:22'),(3,2,'Services','','c3f3bd28d012305ed10f0def8bd35b3d.jpg','publish','2017-10-01 12:51:55'),(4,5,'','','47f9004e9bd83c1d8a9644ffe2a4339b.jpeg','publish','2017-10-01 16:28:57');

/*Table structure for table `cms_category` */

DROP TABLE IF EXISTS `cms_category`;

CREATE TABLE `cms_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_title` varchar(255) NOT NULL,
  `category_description` text NOT NULL,
  `category_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `cms_category` */

insert  into `cms_category`(`id`,`category_title`,`category_description`,`category_date`,`category_slug`) values (1,'News','','2017-09-30 23:58:14','news'),(2,'Test Category','','2017-10-01 02:41:05','cat'),(3,'Services','','2017-10-01 16:31:18','services'),(4,'People','','2017-10-03 17:18:56','people');

/*Table structure for table `cms_category_relationship` */

DROP TABLE IF EXISTS `cms_category_relationship`;

CREATE TABLE `cms_category_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `cms_category_relationship` */

insert  into `cms_category_relationship`(`id`,`category_id`,`menu_id`) values (1,3,16),(2,3,18),(3,3,19),(4,3,20),(5,3,20),(6,3,20),(7,3,20),(8,3,20),(9,3,20),(10,3,20),(11,3,20),(12,3,16),(13,3,16),(14,3,22);

/*Table structure for table `cms_menu` */

DROP TABLE IF EXISTS `cms_menu`;

CREATE TABLE `cms_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_category` int(11) NOT NULL,
  `menu_title` varchar(255) NOT NULL,
  `menu_url` varchar(255) NOT NULL,
  `menu_class` varchar(255) NOT NULL,
  `menu_type` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cms_menu` */

/*Table structure for table `cms_menu_category` */

DROP TABLE IF EXISTS `cms_menu_category`;

CREATE TABLE `cms_menu_category` (
  `menu_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_category_title` varchar(255) NOT NULL,
  `menu_category_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`menu_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `cms_menu_category` */

insert  into `cms_menu_category`(`menu_category_id`,`menu_category_title`,`menu_category_date`) values (1,'Top Nav','2017-09-30 23:38:31'),(2,'Middle Nav','2017-09-30 23:38:42'),(3,'Footer Nav','2017-09-30 23:38:47');

/*Table structure for table `cms_menu_relationship` */

DROP TABLE IF EXISTS `cms_menu_relationship`;

CREATE TABLE `cms_menu_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `menu_category_id` int(11) NOT NULL,
  `menu_sort` int(11) NOT NULL,
  `menu_type` varchar(255) NOT NULL,
  `menu_parent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `cms_menu_relationship` */

insert  into `cms_menu_relationship`(`id`,`menu_id`,`menu_category_id`,`menu_sort`,`menu_type`,`menu_parent`) values (1,1,2,1,'category',0),(3,3,2,3,'page',0),(4,4,2,4,'page',0),(5,5,2,5,'page',0),(6,6,2,6,'page',0),(7,7,2,7,'page',0),(10,8,2,8,'page',0),(18,2,2,0,'page',0),(33,21,2,0,'page',0),(34,20,2,0,'post',0),(35,16,2,0,'post',3);

/*Table structure for table `cms_option` */

DROP TABLE IF EXISTS `cms_option`;

CREATE TABLE `cms_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `cms_option` */

insert  into `cms_option`(`id`,`title`) values (1,'Moore');

/*Table structure for table `cms_people` */

DROP TABLE IF EXISTS `cms_people`;

CREATE TABLE `cms_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(255) NOT NULL,
  `p_slug` varchar(255) NOT NULL,
  `p_desc` text NOT NULL,
  `p_image` varchar(255) NOT NULL,
  `p_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `p_contact` varchar(255) NOT NULL,
  `p_email` varchar(255) NOT NULL,
  `p_address` varchar(500) NOT NULL,
  `p_position` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `cms_people` */

insert  into `cms_people`(`id`,`p_name`,`p_slug`,`p_desc`,`p_image`,`p_date`,`p_contact`,`p_email`,`p_address`,`p_position`) values (1,'Emmanuel Y. Mendoza','emmanuel-y-mendoza','<p>Mr. Mendoza started his career and spent 10 years with the Financial Services Group of the leading accounting firm in the Philippines, SyCip Gorres Velayo &amp; Co (SGV&amp;Co.). He was the Deputy Managing Partner of a local accounting firm and headed the Business Consulting and Tax Group for three years. He has extensive experience in auditing financial institutions, as well as in due diligence review for mergers and acquisitions, bond offerings, initial public offerings and investment valuation. Together with a group of Arthur Andersen risk management experts, he was involved in the preparation of Derivatives Risk Control Manual and Risk Management System for certain commercial banks. He also acted as a consultant in the formulation of a transfer pricing system for one of the biggest commercial banks.</p>\r\n\r\n<p>On several occasions, he was commissioned by the Court of Tax Appeals as an independent certified public accountant tasked to verify the voluminous supporting documents of Companies seeking for tax refund on their unutilized input tax credit. He has also prepared tax manuals for certain clients.</p>\r\n\r\n<p>Aside from his exposure to public practice, he was also directly involved in banking operations, as First Vice President and Financial Controller of Global Business Bank (GLOBALBANK), an affiliate of Metrobank and the Tokai Bank of Japan.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Mr. Mendoza also served as GLOBALBANK&rsquo;s Deputy Compliance Officer and Liaison Officer with the Bangko Sentral ng Pilipinas and was a member of the Bank&rsquo;s Asset Liability Committee (ALCO) and the Operations and Compliance Committee. He played a strategic role in the recent three-way merger and acquisition between GLOBALBANK, Philippine Banking Corporation and Asianbank Corporation, particularly in the areas of regulatory requirements, tax planning, computer system integration, and in policies and procedures integration. As Controller, he also oversees the Bank&rsquo;s Risk Management Unit and was involved in the preparation of its risk management manual.</p>\r\n\r\n<p>Mr. Mendoza is a graduate of Bachelor in Business Administration (BBA) in Accountancy from the University of the Philippines and took his Master in Management from the Asian Institute of Management. He is a Certified Public Accountant and is a member of the Philippine Institute of Certified Public Accountants (PICPA) and the Association of Certified Public Accountants in Public Practice (ACPAPP). He is also a member of the Bank Institute of the Philippines.</p>\r\n','','2017-10-04 18:59:21','(632) 887-1888','eymendoza@mqc.com.ph','Makati','Managing Partner'),(2,'Richard S. Querido','richard-s-querido','Mr. Querido started his career and spent 10 years with SGV&amp;Co. and 3 years as a Partner in a local accounting firm. He performed audit and business advisory services of various companies in the fields of food processing and distribution, financial markets, manufacturing and distribution of consumer products and garments, telecommunications, real estate, hotels, and airlines, among others. He managed various local and international engagements, including operations review and tax planning and compliance.<br />\r\nMr. Querido participated extensively in a due diligence review for a cross border listing in the Singapore Stock Exchange. He also served as Technical Advisor in Jakarta, Indonesia as manager of various audit engagements.<br />\r\n&nbsp;<br />\r\n&nbsp;<br />\r\nHe graduated Cum Laude from the Polytechnic University of the Philippines, with a degree of Bachelor in Accountancy. He is a Certified Public Accountant and a Certified Internal Auditor. He is a member of the Philippine Institute of Certified Public Accountants (PICPA), the Association of Certified Public Accountants in Public Practice (ACPAPP), the Institute of Internal Auditors (Philippines) and Tax Management Association of the Philippines (TMAP).','','2017-10-04 19:13:54','(632) 887-1888','rsquerido@mqc.com.ph','','Partner'),(3,'Jekell G. Salosagcol ','jekell-g-salosagcol ','Mr. Salosagcol a former staff auditor in the leading accounting firm in the Philippines, SyCip Gorres Velayo &amp; Co. (SGV&amp;Co.) from 1995 to 1996. Performed financial audits for different clients, most of which are in the banking and telecommunication industry. He is a former consultant of Independent Oil Price Review Committee &ndash; Department of Energy and Vice president for US Operations in Theorem Global Business Outsourcing Inc. Presently he works as a lecturer and reviewer for leading university and review schools in the country. He also served as a resource speaker at the Philippine Institute of Certified Public Accountants and in-house seminar provider for corporate clients such as Manila Electric Company, Commission on Audit, Bureau of Treasury, Bureau of Internal Revenue, Asian Institute of Taxation, Accenture Philippines, Dole Philippines, Philippines Association of Local Government Accountants and National Institute of Accounting Technician. Moreover he is currently the technical and training consultant of Mendoza Querido &amp; Co. and Reyes Tacandong &amp; Co., he is also the Audit and PFRS consultant of Government Service Insurance System (GSIS) and Consultant of ORT Consulting Group LLC in Chicago USA. He has authored two books: with Petronilo Santos on Basic Auditing Theory and Concepts; and A guide in understanding the Philippine Standards on Auditing which has been reprinted for its second &amp; third editions. He graduated with honors from Technological Institute of the Philippines and ranked number 2 (2nd placer) in the May 1995 Philippine CPA Licensure Examination. He is a member of the Philippine Institute of Certified Public Accountants (PICPA).','','2017-10-04 19:14:55','','','','External Consultant'),(4,'Lois Cookea','lois-cookea','TEst','','2017-11-06 22:01:34','Rerum pariatur Eum sint debitis eius similique autem aliquam ipsam in commodo unde velit sit commodo in voluptas','biqygupa@gmail.com','Nulla esse voluptatem dicta praesentium eaque velit fugiat tempore quia','Rem sunt rerum sint sit reiciendis voluptate optio omnis nemo eos neque ipsam voluptate occaecat qui voluptatem sunt debitis');

/*Table structure for table `cms_post` */

DROP TABLE IF EXISTS `cms_post`;

CREATE TABLE `cms_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `page_title` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_description` text NOT NULL,
  `page_status` varchar(255) NOT NULL DEFAULT '0' COMMENT '0 = DRAFT , 1 = PAGE , 2 = POST',
  `page_type` varchar(255) NOT NULL DEFAULT 'page' COMMENT 'draft',
  `page_template` varchar(255) NOT NULL,
  `page_feature` varchar(255) NOT NULL,
  `page_sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `cms_post` */

insert  into `cms_post`(`id`,`page_date`,`page_title`,`page_slug`,`page_description`,`page_status`,`page_type`,`page_template`,`page_feature`,`page_sort`) values (2,'2017-09-30 23:32:33','Services','services','Our global network of more than 20,000 people enables you to access a comprehensive range of services, skills and up to date practical experience of the latest trends and issues affecting the global market place, as well as the local technical knowledge required to add real commercial value to you and your business.<br />\n<br />\nSo, whether you are just starting out in business or about to internationalise, Moore Stephens International member firms deliver a range of integrated services to help you grow, realise and protect your wealth, both in your home country and internationally.<br />\n<br />\nTake a look at the services offered by our member firms to find out about the value and benefits you can expect and locate your nearest Moore Stephens International member firm.&nbsp;','publish','page','column3','',0),(3,'2017-09-30 23:35:20','Sectors','sectors','Moore Stephens International member firms&#39;&nbsp;focus on excellence in key business sectors and their needs enables them to deliver practical, commercial, bespoke solutions to your own unique issues.<br />\n<br />\nGood advice benefits all businesses, so Moore Stephens International members&#39;&nbsp;clients come from a wide variety of backgrounds. But, if one feature characterises them all, it&#39;s the dynamic and complex nature of their financial interests that span national boundaries and different business sectors. That&#39;s why member firms provide a depth as well as a breadth of expertise, which is at the core of their strategy to be the best in their chosen markets.<br />\n<br />\nThey achieve that depth by encouraging their people to develop industry specific expertise.&nbsp;<br />\n<br />\nTo find out about just a few of our &#39;chosen sectors&#39;&nbsp;click on the list, or contact your nearest Moore Stephens International member firm.&nbsp;','publish','page','column3','',0),(4,'2017-09-30 23:35:42','Location','location','<h2>Moore Stephens International Regional Offices</h2>\n<strong>Please note:</strong>&nbsp;This page lists the firms and companies that are members of Moore Stephens International Limited, correspondent firms and, in the case of North America, all listed firms are members of Moore Stephens North America, which is itself a member of Moore Stephens International Limited.<br />\nThe day to day administration of Moore Stephens International Limited is covered by a team based in London &ndash; details can be found&nbsp;here.','publish','page','fullwidth','',0),(5,'2017-09-30 23:36:48','People','people','Below you will find the initial point of contact for the main Moore Stephens International network locations around the world.','publish','page','fullwidth','',0),(6,'2017-09-30 23:37:04','About','about','<h5>Richard Moore &ndash;&nbsp;Chairman of Moore Stephens International Limited</h5>\n\n<p>Moore Stephens International Limited is a global accountancy and consultancy&nbsp;network with its headquarters in London.</p>\n\n<p>Since Moore Stephens London was founded a century ago, Moore Stephens International Limited has grown to be one of the largest international accounting and consulting groups worldwide. Today the&nbsp;network comprises 626 offices in&nbsp;108 countries throughout the world, incorporating 27,997 people and with fees of more than US$2.742 billion. You can be confident that we have the resources and capabilities to meet your needs.<br />\n<br />\nManaging audits and dealing with multi-jurisdictional tax matters of multi-national operations is the core of our business. The scope of our global client management extends, therefore, beyond the delivery of compliance services to advising on international business structures and tax planning to minimise tax liabilities.</p>\n','publish','page','column3','',0),(7,'2017-09-30 23:37:23','Contact Us','contact-us','<h1>&nbsp;</h1>\n\n<h3>Please fill out this form as fully as possible, and we will arrange a relevant member firm to contact you.</h3>\n\n<h4>For careers enquiries please contact the individual member firms direct to find out about vacancies. Find member firms&nbsp;<strong>here</strong></h4>\n\n<h4>Please note that job applications made via this form will not be responded to.</h4>\n','publish','page','column3','',0),(8,'2017-09-30 23:38:01','Join Us','join-us','<h4>Do you have what it takes to be a member firm of Moore Stephens International?</h4>\n<br />\nIf you share our purpose and values why not consider joining us?<br />\n<br />\nMoore Stephens International uses member selection criteria based on quality control, experience and professional service standards across a range of disciplines. If you can meet these then we can offer you the range of benefits that only a comprehensive worldwide network can deliver.<br />\n<br />\n<br />\nIf you would like to find out more, then please provide us with some basic information about your firm -&nbsp;candidate firm form','publish','page','column3','',0),(13,'2017-10-01 01:13:18','News & Views','news','','publish','page','column3','',0),(16,'2017-10-10 23:45:00','Audit & Assurance','audit-and-assurance','<p>We recognise the commercial importance of providing assurance on your business controls and ultimately, satisfying regulatory requirements. We offer much more than just a basic compliance service. We understand the need to provide advice to help you develop your business and achieve your commercial objectives. Local and international standard-setters are constantly enhancing and adapting the reporting requirements to keep up with rapid business and economic developments, as well as to enhance global uniformity in financial reporting. These changes have wide-ranging impact and can affect accounting even for common transaction.</p>\n\n<p>At Moore Stephens, we help your directors, management, and finance teams keep abreast of these changes through constant updates, dialogues, and training. We will help you to identify, understand and address the most critical and challenging financial and reporting issues for your organisation.</p>\n\n<h4>Our Services</h4>\n\n<ul>\n	<li>External audit of financial statements</li>\n	<li>Review of financial statements</li>\n	<li>Acting as Independent Accountants in Initial Public Offerings and Reverse Take-Overs</li>\n	<li>Other assurance and attestation services</li>\n	<li>Compilation of financial information</li>\n	<li>Compliance with regulatory reporting requirements</li>\n</ul>\n','publish','post','innerblog','',0),(17,'2017-10-11 00:21:17','Business Outsourcing Services','business-outsourcing-services','Our Business Outsourcing Services provide a cost effective and efficient way for you to outsource your finance function whilst at the same time ensuring you comply with your statutory and regulatory responsibilities.&nbsp;This enables you to focus on the core aspects of your business.<br />\n<br />\nIn supporting businesses of all sizes, we have gained extensive experience that enables us to efficiently and seamlessly perform the administrative financial functions on behalf of our clients. Our experienced staffs are familiar with major software packages, including&nbsp;<a href=\"https://www.xero.com/sg/\">XERO</a>,&nbsp;<a href=\"http://quickbooks.intuit.com/\">QuickBooks</a>&nbsp;and MYOB ensuring we can deliver accounting services working to your existing financial systems.\n<h2>Our Services</h2>\n\n<ul>\n	<li>Bookkeeping and GST Reporting for Statutory Filing</li>\n	<li>Budgeting, Forecasting and Financial Modeling</li>\n	<li>Cloud Accounting and Support</li>\n	<li>Compilation of Financial Statements</li>\n	<li>extensible Business Reporting Language&nbsp;(XBRL) Filing</li>\n	<li>Management Reporting</li>\n	<li>Payroll Processing</li>\n</ul>\n','publish','post','column3','',0),(18,'2017-10-11 00:38:54','Business Advisory Services','business-advisory-services','Successful organisations need a range of advisory services to support their long term growth plans.&nbsp;By developing an in-depth understanding of your business and the issues and challenges that you face, our advisory team use local and global knowledge to serve your needs.<br />\nWe will help you to identify and develop new businesses, penetrate new markets, form new partnerships, and leapfrog your competitors through synergistic mergers and acquisitions. Pertaining to growth capital, our specialists will help you identify, evaluate, select and open the doors to wide variety of financing options including debt and equity capital markets.<br />\n<br />\nOur Services\n<ul>\n	<li>Business Consulting and Strategic Planning&nbsp;</li>\n	<li>Business Structuring and Financing&nbsp;</li>\n	<li>Corporate Governance Review&nbsp;</li>\n	<li>Corporate Secretarial Services&nbsp;</li>\n	<li>Human Resources Management&nbsp;</li>\n	<li>IT Solutions</li>\n	<li>Outsourced Accounting Services&nbsp;</li>\n	<li>Tax Compliance</li>\n	<li>Tax Planning&nbsp;</li>\n</ul>\n','publish','post','column3','',0),(19,'2017-10-11 00:39:30','Corporate Finance','corporate-finance','Whether you are looking to raise development capital or float on a public market, make an acquisition, form a strategic alliance or release funds through a sale or restructuring, our corporate finance specialists can help you maximise the returns, while advising and supporting you every step of the way.&nbsp;<br />\n<br />\nOur aim is to work closely with you to achieve your corporate and strategic goals.&nbsp; Assisted by teams located in all major cities in the world, we work with companies who require our expertise in the fields of mergers and acquisitions, equity capital markets and other financial advisory work.<br />\n<br />\nYou can rely on us for trusted support in mergers, acquisitions and disposals. Regardless of whether you are the buyer or the seller, our specialists will manage your entire transaction to ensure successful completion.&nbsp; We can help you by establishing pricing expectations and will structure your deal taking into consideration the local governing rules and regulations.&nbsp; If your company is keen to expand or thinking of strategically divesting the business, you can leverage from the wide range of services that we offer in relation to business and financial analyses, valuations, information memoranda, due diligence, post-merger integration and much more.<br />\n<br />\nWe&nbsp;provide practical advice on the full range of corporate finance, lead advisory and transaction support services - from pre-deal evaluation through to completion and post deal integration or separation.<br />\n<br />\nOur Services\n<ul>\n	<li>Business Forecasting&nbsp;</li>\n	<li>Financial Due Diligence</li>\n	<li>Initial Public Offering</li>\n	<li>Reverse Take-overs</li>\n	<li>Securing Funding&nbsp;</li>\n	<li>Business Valuations&nbsp;</li>\n	<li>Management Buy-Outs and Buy-Ins&nbsp;</li>\n	<li>Securing your Business&nbsp;</li>\n	<li>Creating A Strategic Plan&nbsp;</li>\n	<li>Mergers &amp; Acquisitions&nbsp;</li>\n	<li>Strategic Advice&nbsp;</li>\n</ul>\n<br />\n&nbsp;','publish','post','column3','',0),(20,'2017-10-11 00:39:55','Taxation','taxation-2','There are tax implications for whatever we do in today&#39;s world! Tax is an inescapable element of almost every business activity and in every jurisdiction where you conduct business. Decisions made today will often affect your tax burden in the future.<br />\n<br />\nMember firms of Moore Stephens help you achieve maximum tax-efficiency in the short and long-term by providing a range of local tax services and advice, including:\n<ul>\n	<li>&nbsp;corporate and personal tax compliance</li>\n	<li>&nbsp;corporate and personal tax planning, consultancy and structuring</li>\n	<li>&nbsp;trust and estate planning</li>\n	<li>&nbsp;employer solutions, including payroll and remuneration planning</li>\n	<li>&nbsp;tax investigations support</li>\n</ul>\n<br />\nThe growth in the global economy means an increase in cross-border transactions. With foreign tax legislation comes a wealth of opportunities and pitfalls in structuring your supply lines and operating structure around the globe. International tax solutions go far deeper than merely direct tax, with issues such as foreign custom duties, VAT and transfer pricing also requiring attention.:<br />\n<br />\nOur international network of member firms provides access to a comprehensive international tax planning service and all the international tax and commercial information you need to enable you to grow your global business successfully.<br />\nThis includes advice on:\n<ul>\n	<li>&nbsp;international group and financing structures</li>\n	<li>&nbsp;cross-border transactions and transfer pricing</li>\n	<li>&nbsp;international trading</li>\n	<li>&nbsp;corporate residence</li>\n	<li>&nbsp;trading in or with a country</li>\n	<li>&nbsp;branches, agencies and service companies</li>\n	<li>&nbsp;inward and outward investment</li>\n	<li>&nbsp;protection of assets, income and capital gains using overseas structures and trusts</li>\n	<li>&nbsp;international employment arrangements</li>\n	<li>&nbsp;intellectual property</li>\n	<li>&nbsp;international private client services</li>\n	<li>&nbsp;detailed local advice and support</li>\n</ul>\n<br />\n&nbsp;','publish','post','column3','',0),(21,'2017-10-11 00:54:32','Career','career','','publish','page','fullwidth','',0),(22,'2017-11-15 02:14:38','Test Job 4','test','Test','publish','post','column2','',0);

/*Table structure for table `cms_tags` */

DROP TABLE IF EXISTS `cms_tags`;

CREATE TABLE `cms_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_title` varchar(255) NOT NULL,
  `tag_alias` varchar(255) NOT NULL,
  `tag_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cms_tags` */

/*Table structure for table `cms_widget` */

DROP TABLE IF EXISTS `cms_widget`;

CREATE TABLE `cms_widget` (
  `w_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_title` varchar(255) NOT NULL,
  `w_description` text NOT NULL,
  PRIMARY KEY (`w_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cms_widget` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
