(function() {


$(document).ready(function() {
  $('body').on('click', function() {
    $('.main-nav .pull-left, body').removeClass('nav-active');
    $('.main-nav .search').removeClass('active');
  })

  $('.hero-slider .slide-container').on('init', function(slick) {
    $('.hero-slider .slide-container .slide').removeClass('hidden');
  })

  $('.hero-slider .slide-container').slick({
    nextArrow: $('.slide-control .arrow.left'),
    prevArrow: $('.slide-control .arrow.right'),
    responsive: [
      {
        breakpoint: 768,
        settings: {
          dots: true
        }
      }
    ]
  });

  $('.main-nav .fa-search').on('click', function(e) {
    e.stopPropagation();
    $('.main-nav .search').toggleClass('active');
  })


  $('.content-left-toggle').on('click', function(e) {
    e.preventDefault();

    $('.content-left').toggle(150, function() {
      $('.content-left').toggleClass('active');
    });
  })



  $('.main-nav .toggle').on('click', function(e) {
    e.stopPropagation();
    $('.main-nav .pull-left, body').toggleClass('nav-active');
  })

  $('.main-nav .pull-left, .main-nav .search input').on('click', function(e) {
    e.stopPropagation();
  })
})


})();
