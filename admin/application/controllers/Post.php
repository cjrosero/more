<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Main');
	}

	public function index()
	{

		$post = $this->uri->segment(2);

		switch ($post) {
			case 'new-post':
			$this->__new_post();
			break;

			case 'edit-post':
			$this->__edit_post();
			break;
			
			default:
			$this->__index_post();
			break;
		}
	}

	public function __index_post(){
		$data['info'] = array(
			'vfile'			 	=> 'post/post_index',
			'title'			 	=> 'post',
			'controller_name'	=> 'post',
			'link' 			 	=> base_url('post'),
			'page_name' 	 	=> '',
			'post_nav' => 'active'
		);

		$data['posts'] = $this->Main->select('*','cms_post',array('page_status'=>'publish','page_type'=>'post'),0,array('col'=>'page_date','sort'=>'ASC'));


		$this->load->view('template',$data);
	}

	public function __new_post(){
		$data['info'] = array(
			'vfile'			 	=> 'post/post_new',
			'title'			 	=> 'New post',
			'controller_name'	=> 'post',
			'link' 			 	=> base_url('post'),
			'page_name' 	 	=> '',
			'post_nav' => 'active'
		);
		$data['categories'] = $this->Main->select('*','cms_category');
		$this->load->view('template',$data);

	}

	public function browse(){
		
		$this->load->view('browse');
	}

	public function uploads(){
		$callback = 'null';
		$url = '';
		$get = array();
		$msg = '';

		$qry = $_SERVER['REQUEST_URI'];
		parse_str(substr($qry, strpos($qry, '?') + 1), $get);

		if (!isset($_POST) || !isset($get['CKEditorFuncNum'])) {
			$msg = 'CKEditor instance not defined. Cannot upload image.';
		} else {
			$callback = $get['CKEditorFuncNum'];

			try {

				$config['upload_path']          = FCPATH.'/uploads/events/';
				$config['allowed_types']        = 'gif|jpg|png';
				// $config['max_size']             = 3000;
				$config['encrypt_name']			= TRUE;


				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('upload'))
				{
					$error = array('error' => $this->upload->display_errors());

					$msg = $this->upload->display_errors();
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());


					$url = base_url() .'uploads/events/'. $data['upload_data']['file_name'];
					$msg = "File uploaded successfully to: {$url}";

				}

			} catch (Exception $e) {
				$url = '';
				$msg = $e->getMessage();
			}
		}

        // Callback function that inserts image into correct CKEditor instance
		$output = '<html><body><script type="text/javascript">' .
		'window.parent.CKEDITOR.tools.callFunction(' .
		$callback .
		', "' .
		$url .
		'", "' .
		$msg .
		'");</script></body></html>';

		echo $output;
	}

	public function __edit_post(){
		$data['info'] = array(
			'vfile'			 	=> 'post/post_edit',
			'title'			 	=> 'Edit post',
			'controller_name'	=> 'post',
			'link' 			 	=> base_url('post'),
			'page_name' 	 	=> '',
			'post_nav' => 'active'
		);
		$id = $this->input->get('num_page');
		
		$data['categories'] = $this->Main->select('*','cms_category');
		$cat_rel =  $this->Main->select('*','cms_category_relationship',array('menu_id' => $id));
		$caty = array();
		if (!empty($cat_rel)) {
			foreach ($cat_rel as $cats) {
				$caty[$cats->category_id] = array('checked' => 'checked');
			}
		}

		$data['caty'] = $caty;
		 // echo  "<pre>";var_dump($caty);die();

		
		$data['content'] = $this->Main->select('*','cms_post',array('id' => $id),1);

		$this->load->view('template',$data);
	}

}

/* End of file post.php */
/* Location: ./application/controllers/post.php */