<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class People extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Main');
	}

	public function index()
	{

		$people = $this->uri->segment(2);
		$id = $this->uri->segment(3);

		switch ($people) {
			case 'new-people':
			$this->__new_people();
			break;
			case 'edit-people':
			$this->__edit_people();
			break;
			default:
			$this->__index_people();
			break;
		}
	}

	public function __index_people(){
		$data['info'] = array(
			'vfile'			 	=> 'people/people_index',
			'title'			 	=> 'people',
			'controller_name'	=> 'people',
			'link' 			 	=> base_url('people'),
			'page_name' 	 	=> '',
			'people_menu' => 'active'
		);
		$page_exisit = "0,";
		$page_exisit = rtrim(',',$page_exisit);
		$data['peoples'] = $this->Main->raw("SELECT * FROM `cms_people`");

		$data['page'] = $this->Main->select('*','cms_post',array('page_status'=>'publish','page_type'=>'page'),0,array('col'=>'page_date','sort'=>'ASC'));
		// if (!empty($data['page'])) {
		// 	foreach ($data['page'] as $p) {
		// 		$page_exisit .= $p.","
		// 	}


		// }
		

		$this->load->view('template',$data);
	}

	public function __new_people(){
		$data['info'] = array(
			'vfile'			 	=> 'people/people_new',
			'title'			 	=> 'New people',
			'controller_name'	=> 'people',
			'link' 			 	=> base_url('people'),
			'page_name' 	 	=> '',
			'people_menu' => 'active'
		);

		$this->load->view('template',$data);

	}

	public function browse(){
		
		$this->load->view('browse');
	}

	public function uploads(){
		$callback = 'null';
		$url = '';
		$get = array();
		$msg = '';

		$qry = $_SERVER['REQUEST_URI'];
		parse_str(substr($qry, strpos($qry, '?') + 1), $get);

		if (!isset($_POST) || !isset($get['CKEditorFuncNum'])) {
			$msg = 'CKEditor instance not defined. Cannot upload image.';
		} else {
			$callback = $get['CKEditorFuncNum'];

			try {

				$config['upload_path']          = FCPATH.'/uploads/cms/';
				$config['allowed_types']        = 'gif|jpg|png';
				// $config['max_size']             = 3000;
				$config['encrypt_name']			= TRUE;


				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('upload'))
				{
					$error = array('error' => $this->upload->display_errors());

					$msg = $this->upload->display_errors();
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());


					$url = base_url() .'uploads/events/'. $data['upload_data']['file_name'];
					$msg = "File uploaded successfully to: {$url}";

				}

			} catch (Exception $e) {
				$url = '';
				$msg = $e->getMessage();
			}
		}

        // Callback function that inserts image into correct CKEditor instance
		$output = '<html><body><script type="text/javascript">' .
		'window.parent.CKEDITOR.tools.callFunction(' .
		$callback .
		', "' .
		$url .
		'", "' .
		$msg .
		'");</script></body></html>';

		echo $output;
	}

	public function __edit_people(){
		$data['info'] = array(
			'vfile'			 	=> 'people/people_edit',
			'title'			 	=> 'Edit people',
			'controller_name'	=> 'people',
			'link' 			 	=> base_url('people'),
			'page_name' 	 	=> '',
			'people_nav' => 'active'
		);

		$id = $this->input->get('num_people');
		echo $id;

		// $data['content'] = $this->Main->select('*','cms_post',array('id' => $id),1);

		$this->load->view('template',$data);
	}

}

/* End of file people.php */
/* Location: ./application/controllers/people.php */