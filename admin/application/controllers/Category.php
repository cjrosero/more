<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Main');
	}

	public function index()
	{

		$category = $this->uri->segment(2);
		$id = $this->uri->segment(3);

		switch ($category) {
			case 'new-category':
			$this->__new_category();
			break;
			case 'edit-category':
			echo "aw";
			$this->__edit_category();
			break;
			default:
			$this->__index_category();
			break;
		}
	}

	public function __index_category(){
		$data['info'] = array(
			'vfile'			 	=> 'category/category_index',
			'title'			 	=> 'category',
			'controller_name'	=> 'category',
			'link' 			 	=> base_url('category'),
			'page_name' 	 	=> 'Category',
			'category_menu' => 'active'
		);

		$data['categories'] = $this->Main->select('*','cms_category');

		$this->load->view('template',$data);
	}

	public function __new_category(){
		$data['info'] = array(
			'vfile'			 	=> 'category/category_new',
			'title'			 	=> 'New category',
			'controller_name'	=> 'category',
			'link' 			 	=> base_url('category'),
			'page_name' 	 	=> '',
			'category_nav' => 'active'
		);

		$this->load->view('template',$data);

	}

	public function browse(){
		
		$this->load->view('browse');
	}

	public function uploads(){
		$callback = 'null';
		$url = '';
		$get = array();
		$msg = '';

		$qry = $_SERVER['REQUEST_URI'];
		parse_str(substr($qry, strpos($qry, '?') + 1), $get);

		if (!isset($_POST) || !isset($get['CKEditorFuncNum'])) {
			$msg = 'CKEditor instance not defined. Cannot upload image.';
		} else {
			$callback = $get['CKEditorFuncNum'];

			try {

				$config['upload_path']          = FCPATH.'/uploads/events/';
				$config['allowed_types']        = 'gif|jpg|png';
				// $config['max_size']             = 3000;
				$config['encrypt_name']			= TRUE;


				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('upload'))
				{
					$error = array('error' => $this->upload->display_errors());

					$msg = $this->upload->display_errors();
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());


					$url = base_url() .'uploads/events/'. $data['upload_data']['file_name'];
					$msg = "File uploaded successfully to: {$url}";

				}

			} catch (Exception $e) {
				$url = '';
				$msg = $e->getMessage();
			}
		}

        // Callback function that inserts image into correct CKEditor instance
		$output = '<html><body><script type="text/javascript">' .
		'window.parent.CKEDITOR.tools.callFunction(' .
		$callback .
		', "' .
		$url .
		'", "' .
		$msg .
		'");</script></body></html>';

		echo $output;
	}

	public function __edit_category(){
		$data['info'] = array(
			'vfile'			 	=> 'category/category_edit',
			'title'			 	=> 'Edit category',
			'controller_name'	=> 'category',
			'link' 			 	=> base_url('category'),
			'page_name' 	 	=> '',
			'category_nav' => 'active'
		);

		$id = $this->input->get('num_page');
		$data['categories'] = $this->Main->select('*','cms_category');
		

		$data['content'] = $this->Main->select('*','cms_category',array('id' => $id),1);
		if (empty($data['content'])) {
			redirect('category');
		}
	
		$this->load->view('template',$data);
	}

}

