<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Main');
	}

	public function index()
	{

		$page = $this->uri->segment(2);
		$id = $this->uri->segment(3);

		switch ($page) {
			case 'new-page':
			$this->__new_page();
			break;
			case 'edit-page':
			$this->__edit_page();
			break;
			default:
			$this->__index_page();
			break;
		}
	}

	public function __index_page(){
		$data['info'] = array(
			'vfile'			 	=> 'page/page_index',
			'title'			 	=> 'Page',
			'controller_name'	=> 'Page',
			'link' 			 	=> base_url('page'),
			'page_name' 	 	=> '',
			'page_nav' => 'active'
		);

		$data['pages'] = $this->Main->select('*','cms_post',array('page_type'=>'page'),0,array('col'=>'page_date','sort'=>'ASC'));

		$this->load->view('template',$data);
	}

	public function __new_page(){
		$data['info'] = array(
			'vfile'			 	=> 'page/page_new',
			'title'			 	=> 'New Page',
			'controller_name'	=> 'Page',
			'link' 			 	=> base_url('page'),
			'page_name' 	 	=> '',
			'page_nav' => 'active'
		);

		$this->load->view('template',$data);

	}

	public function browse(){
		
		$this->load->view('browse');
	}

	public function uploads(){
		$callback = 'null';
		$url = '';
		$get = array();
		$msg = '';

		$qry = $_SERVER['REQUEST_URI'];
		parse_str(substr($qry, strpos($qry, '?') + 1), $get);

		if (!isset($_POST) || !isset($get['CKEditorFuncNum'])) {
			$msg = 'CKEditor instance not defined. Cannot upload image.';
		} else {
			$callback = $get['CKEditorFuncNum'];

			try {

				$config['upload_path']          = FCPATH.'/uploads/events/';
				$config['allowed_types']        = 'gif|jpg|png';
				// $config['max_size']             = 3000;
				$config['encrypt_name']			= TRUE;


				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('upload'))
				{
					$error = array('error' => $this->upload->display_errors());

					$msg = $this->upload->display_errors();
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());


					$url = base_url() .'uploads/events/'. $data['upload_data']['file_name'];
					$msg = "File uploaded successfully to: {$url}";

				}

			} catch (Exception $e) {
				$url = '';
				$msg = $e->getMessage();
			}
		}

        // Callback function that inserts image into correct CKEditor instance
		$output = '<html><body><script type="text/javascript">' .
		'window.parent.CKEDITOR.tools.callFunction(' .
		$callback .
		', "' .
		$url .
		'", "' .
		$msg .
		'");</script></body></html>';

		echo $output;
	}

	public function __edit_page(){
		$data['info'] = array(
			'vfile'			 	=> 'page/page_edit',
			'title'			 	=> 'Edit Page',
			'controller_name'	=> 'Page',
			'link' 			 	=> base_url('page'),
			'page_name' 	 	=> '',
			'page_nav' => 'active'
		);

		$id = $this->input->get('num_page');
		echo $id;

		$data['content'] = $this->Main->select('*','cms_post',array('id' => $id),1);
	
		$this->load->view('template',$data);
	}

}

/* End of file Page.php */
/* Location: ./application/controllers/Page.php */