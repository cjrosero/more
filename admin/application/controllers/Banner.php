<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Main');
	}

	public function index()
	{

		$banner = $this->uri->segment(2);
		$id = $this->uri->segment(3);

		switch ($banner) {
			case 'new-banner':
			$this->__new_banner();
			break;
			case 'edit-banner':
			$this->__edit_banner();
			break;
			default:
			$this->__index_banner();
			break;
		}
	}

	public function __index_banner(){
		$data['info'] = array(
			'vfile'			 	=> 'banner/banner_index',
			'title'			 	=> 'banner',
			'controller_name'	=> 'banner',
			'link' 			 	=> base_url('banner'),
			'page_name' 	 	=> '',
			'banner_menu' => 'active'
		);
		$page_exisit = "0,";
		$page_exisit = rtrim(',',$page_exisit);
		$data['banners'] = $this->Main->raw("SELECT * FROM `cms_banner`");

		$data['page'] = $this->Main->select('*','cms_post',array('page_status'=>'publish','page_type'=>'page'),0,array('col'=>'page_date','sort'=>'ASC'));
		// if (!empty($data['page'])) {
		// 	foreach ($data['page'] as $p) {
		// 		$page_exisit .= $p.","
		// 	}


		// }
		

		$this->load->view('template',$data);
	}

	public function __new_banner(){
		$data['info'] = array(
			'vfile'			 	=> 'banner/banner_new',
			'title'			 	=> 'New banner',
			'controller_name'	=> 'banner',
			'link' 			 	=> base_url('banner'),
			'page_name' 	 	=> '',
			'banner_menu' => 'active'
		);

		$this->load->view('template',$data);

	}

	public function browse(){
		
		$this->load->view('browse');
	}

	public function uploads(){
		$callback = 'null';
		$url = '';
		$get = array();
		$msg = '';

		$qry = $_SERVER['REQUEST_URI'];
		parse_str(substr($qry, strpos($qry, '?') + 1), $get);

		if (!isset($_POST) || !isset($get['CKEditorFuncNum'])) {
			$msg = 'CKEditor instance not defined. Cannot upload image.';
		} else {
			$callback = $get['CKEditorFuncNum'];

			try {

				$config['upload_path']          = FCPATH.'/uploads/cms/';
				$config['allowed_types']        = 'gif|jpg|png';
				// $config['max_size']             = 3000;
				$config['encrypt_name']			= TRUE;


				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('upload'))
				{
					$error = array('error' => $this->upload->display_errors());

					$msg = $this->upload->display_errors();
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());


					$url = base_url() .'uploads/events/'. $data['upload_data']['file_name'];
					$msg = "File uploaded successfully to: {$url}";

				}

			} catch (Exception $e) {
				$url = '';
				$msg = $e->getMessage();
			}
		}

        // Callback function that inserts image into correct CKEditor instance
		$output = '<html><body><script type="text/javascript">' .
		'window.parent.CKEDITOR.tools.callFunction(' .
		$callback .
		', "' .
		$url .
		'", "' .
		$msg .
		'");</script></body></html>';

		echo $output;
	}

	public function __edit_banner(){
		$data['info'] = array(
			'vfile'			 	=> 'banner/banner_edit',
			'title'			 	=> 'Edit banner',
			'controller_name'	=> 'banner',
			'link' 			 	=> base_url('banner'),
			'page_name' 	 	=> '',
			'banner_nav' => 'active'
		);

		$id = $this->input->get('num_banner');
		echo $id;

		// $data['content'] = $this->Main->select('*','cms_post',array('id' => $id),1);

		$this->load->view('template',$data);
	}

}

/* End of file banner.php */
/* Location: ./application/controllers/banner.php */