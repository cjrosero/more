<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Main');
	}

	public function index()
	{
		$data['info'] = array(
			'vfile'			 	=> 'menu/menu_index',
			'title'			 	=> 'Menu',
			'controller_name'	=> 'Menu',
			'link' 			 	=> base_url('menu'),
			'page_name' 	 	=> '',
			'page_menu' => 'active'
		);
		$inc = array();
$in = array();
$inc = array();
		$data['menu_categories'] = $this->Main->select('*','cms_menu_category');
		if (!empty($data['menu_categories'])) {
			
			if (empty($_GET['menu_category_settings'])) {
				$data['category_settings_select'] = $data['menu_categories'][0]->menu_category_id;
			}else{
				$data['category_settings_select'] = $_GET['menu_category_settings'];
			}
			$data['menus'] = $this->Main->select('*','cms_menu');
			

			$menu_selected = $this->Main->select('menu_id,menu_type','cms_menu_relationship',array('menu_category_id'=>$data['category_settings_select']));
			if (!empty($menu_selected)) {

				foreach ($menu_selected as $msi ) {
					if ($msi->menu_type == "category") {
						$misc[] = $msi->menu_id;
					
					}else{
						$mis[] = $msi->menu_id;

					}
				}
				// $in = array('menu_id' => 'id','data' => $mis );
				// $inc = array('menu_id' => 'id','data' => $misc );
			}else{
				$mis = array();
				
			}
			
			$data['page_link'] = $this->Main->select('page_title,id','cms_post',array('page_type'=> 'page','page_status'=>'publish'),0,array(),$in);
			$data['post_link'] = $this->Main->select('page_title,id','cms_post',array('page_type'=> 'post','page_status'=>'publish'),0,array(),$in);
			$data['categories'] = $this->Main->select('category_title,id','cms_category',array(),0,array(),$inc);

			// $data['link_select'] = $this->m->raw("SELECT cms_post.*,cms_menu_relationship.menu_id as m_id FROM cms_post
			// 	INNER JOIN cms_menu_relationship
			// 	ON cms_post.`id` = cms_menu_relationship.`menu_id`
			// 	WHERE cms_menu_relationship.`menu_category_id` = '".$data['category_settings_select']."'");

			$data['link_select'] = main_head_nav($data['category_settings_select']);
// echo"<pre>";			var_dump($data['link_select']);die();

			
		}

		

		

		
		$this->load->view('template',$data);
	}

}

/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */