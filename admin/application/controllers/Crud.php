<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Main');
	}
	public function insert_people(){
		$p_name = $this->input->post('p_name');
		$p_contact = $this->input->post('p_contact');
		$p_email = $this->input->post('p_email');
		$desc = $this->input->post('desc');
		$p_position = $this->input->post('p_position');
		$p_address = $this->input->post('p_address');
		$p_slug = $this->input->post('p_slug');

		$config['upload_path']          = BASEU.'/uploads/people/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['encrypt_name'] = TRUE;
		$error = "";
		
		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('uploads')){
			$upload_set = $this->upload->display_errors();
		}else{
			$upload_set=false;
		}
		$upload_data = $this->upload->data(); 
		echo $config['upload_path'];
		$file_path  = "";
		if (!empty($upload_data)) {
		$file_path = $upload_data['file_name'];
			
		}
		$data = array(
			'p_name' => $p_name,
			'p_contact' => $p_contact,
			'p_email' => $p_email,
			'p_desc' => $desc,
			'p_image' => $file_path,
			'p_position' => $p_position,
			'p_address' => $p_address,
			'p_slug' => $p_slug,
		);
		$r = $this->Main->insert('cms_people',$data,true);

	}

	public function insert_page(){
		$table = $this->input->post('table');
		$page_title = $this->input->post('page_title');
		$page_slug = $this->input->post('page_slug');
		$page_description = $this->input->post('page_description');
		$page_status = $this->input->post('page_status');
		$page_template = $this->input->post('page_template');
		$type = $this->input->post('type');
		$page_id = $this->input->post('edit');
		$tags = $this->input->post('tags');
		$updated = false;


		$error = "";
		if (!empty($page_id)) {
			$count_slug = $this->Main->select('page_slug',$table,array('page_slug'=>$page_slug,'id'=>$page_id),1);
			$page_slug = $page_slug;

			
		}else{
			$count_slug = $this->Main->select('page_slug',$table,array('page_slug'=>$page_slug),1);
			if (count($count_slug) >=1) {
				$msg  = array('success' => false, 'msg' => 'Duplicate Link Found please change link' );
				response_json($msg);

				return;

			}else{
				$page_slug = $page_slug;
			}
		}
		$data = array(			
			"page_title" => $page_title,
			"page_slug" => $page_slug,
			"page_description" => $page_description,
			"page_status" => $page_status,
			"page_type" => $type,
			"page_template" => $page_template,
		);
		$check_tag_exisiting = $this->Main->raw("SELECT count(*) as total FROM cms_category_relationship WHERE `menu_id` = '$page_id'",1);
		
		if ($check_tag_exisiting->total >=1) {
			$this->Main->delete('cms_category_relationship',array('menu_id',$page_id));
			
		}
		if (!empty($page_id)) {
			$r = $this->Main->update('cms_post',array('id'=>$page_id),$data,true);
			$updated = true;

			if (!empty($tags)) {
				
				foreach ($tags as $tag) {
					$this->Main->insert('cms_category_relationship',array('category_id'=> $tag,'menu_id'=>$page_id),true);			
				}
			}
			
		}else{
			$r = $this->Main->insert($table,$data,true);
			$lastid = $r['lastid'];
			if (!empty($tags)) {
				foreach ($tags as $tag) {
					$this->Main->insert('cms_category_relationship',array('category_id'=> $tag,'menu_id'=>$lastid),true);			
				}
			}
			
		}

		if ($r['success']) {
			$msg  = array('success' => true, 'msg' => 'Success' ,'type' => $type);
			if ($updated) {
				$msg = array_merge($msg,array('updated'=>true));
			}
		}else{
			$msg  = array('success' => false, 'msg' => validation_errors() );

		}
		response_json($msg);
	}

	public function remove_selected_menu(){
		$id = $this->input->post('id');
		$menu_category_id = $this->input->post('menu_category_id');
		$myid = $this->input->post('myid');

		$data = array('id' => $myid, 'menu_category_id' => $menu_category_id);
		$r = $this->Main->delete('cms_menu_relationship',$data);
		if ($r) {
			if ($r) {
				$msg = array('success' => true,'msg'=> 'Removed From Menu List','myid'=> $myid );
			}else{
				$msg = array('success' => false,'msg'=> 'Something Went Wrong','myid'=> $myid );

			}
		}
		response_json($msg);

	}

	public function insert_menu_category(){
		$menu_category_title = $this->input->post('menu_category_title', TRUE);
		$data = array('menu_category_title' => $menu_category_title, );
		$r = $this->Main->insert('cms_menu_category' , $data);
		if ($r) {
			$msg = array('success' => true,'msg'=> 'New Menu Category Submited!' );
		}else{
			$msg = array('success' => false,'msg'=> 'Something Went Wrong' );

		}
		response_json($msg);


	}

	public function insert_relation_menu(){
		$menu_id = $this->input->post('id');
		$menu_category_id = $this->input->post('menu_category_id');
		$page_title = $this->input->post('page_title');
		$menu_type = $this->input->post('menu_type');
		$data = array('menu_id' => $menu_id, 'menu_category_id' => $menu_category_id ,'menu_type'=>$menu_type);
		$table = "cms_menu_relationship";
		

		$r = $this->Main->insert($table,$data);
		if ($r) {
			$msg  = array('success' => true, 'msg' => 'Successfully Addedd','menu_id' => $menu_id,'page_title'=>$page_title);
		}else{
			$msg  = array('success' => false, 'msg' => 'Something went wrong!' ,'menu_id' => $menu_id,'page_title'=>$page_title);

		}
		response_json($msg);

	}

	public function feature_image(){
		$id = $this->input->post('post_id');
		$config['upload_path']          = FCPATH.'/uploads/feature/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['encrypt_name'] = TRUE;
		$error = "";
		
		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('uploads')){
			$upload_set = true;
		}else{
			$upload_set=false;
		}
		
		$upload_data = $this->upload->data(); 
		$file_name = $upload_data['file_name'];

		$data = array(
			'page_feature' => $file_name ,
			
		);
		$this->Main->update('cms_post',array('id'=>$id),$data);
	}

	public function change_status(){

	}

	public function upload_banner(){
		
		$page = $this->input->post('page');
		$title = $this->input->post('title');
		$status = $this->input->post('status');
		$desc = $this->input->post('desc');

		$config['upload_path']          = BASEU.'/uploads/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['encrypt_name'] = TRUE;
		$error = "";
		
		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('uploads')){
			$upload_set = $this->upload->display_errors();
		}else{
			$upload_set=false;
		}
		
		$upload_data = $this->upload->data(); 
		$file_name = $upload_data['file_name'];

		$data = array(
			'post_id' => $page,
			'banner_title' => $title ,
			'banner_description' => $desc,
			'banner_path' => $file_name,
			'banner_status' => $status,
		);
		var_dump($upload_set);
		$this->Main->insert('cms_banner',$data);
	}

	public function category_insert(){
		$category_title = $this->input->post('category_title');
		$category_slug = $this->input->post('category_slug');
		$category_description = $this->input->post('category_description');
		$id = $this->input->post('id');


		if (empty($category_title) || empty($category_slug)) {
			$error = "";
			if (empty($category_title)) {
				$error .= "Title is missing.</br>";
			}

			if (empty($category_slug)) {
				$error .= "Link is missing.";
				
			}			
		}

		if (!empty($error)) {
			$msg = array('success' => FALSE, 'msg' => $error );

			response_json($msg);
			return;

			
		}
		$data = array(
			'category_title'=>$category_title,
			'category_slug'=>$category_slug,
			'category_description'=>$category_description,

		);
		if (empty($id)) {
			$check_slug = $this->Main->select('count(*) as total','cms_category',array('category_slug' => $category_slug),1)->total;
			$check_title = $this->Main->select('count(*) as total','cms_category',array('category_title' => $category_title),1)->total;

			if ($check_slug >=1 || $check_title >=1) {
				$msg = array('success' => FALSE, 'msg' => 'Title or link already Exisit' );
				response_json($msg);
				return;
			}
			$r = $this->Main->insert('cms_category',$data);
			$message = "Category added";
		}else{
			$r = $this->Main->update('cms_category',array('id'=>$id),$data);
			$message = "Category updated";
		}
		
		


		if ($r) {
			$msg = array('success' => TRUE, 'msg' => $message );
		}else{
			$msg = array('success' => FALSE, 'msg' => 'Something Went Wrong' );
		}

		response_json($msg);

		
	}

}

/* End of file Crud.php */
/* Location: ./application/controllers/Crud.php */