<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'page';
$route['page/(:any)'] = 'page';
$route['post/(:any)'] = 'post';
$route['banner/(:any)'] = 'banner';
$route['category/(:any)'] = 'category';
$route['people/(:any)'] = 'people';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
