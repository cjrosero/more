
<h1>Add New Page</h1>
<div class="row">
	<div class="col md-9 ">

		<form action="page/save" method="POST">
			<div class="card-body">
				<div class="form-group">
					<label for="">Title:</label>
					<input type="text" class="form-control" name="title" id = "title">
					<label for="">Link:</label><input type="text" name = "slug" value="" id = "slug_input" class="form-control slug_input">
					
				</div>
				<div class="form-group">
					<textarea name="desc" id="desc" cols="30" rows="25"></textarea>
				</div>
			</div>
		</form>

	</div>

	<div class="col-md-3">
		
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Page Option	
				</div>
				<div class="card-body">
					<div class="form-group">
						<label for="">Status:</label>
						<select name="page_status" id="page_status" class="form-control">
							<option value="draft">Draft</option>
							<option value="publish">Publish</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Last Update:</label><span>test</span>
					</div>
					<div class="form-group">
						<button class="btn btn-primary btn-block" id = "save_page" >Save</button>
						
					</div>
				</div>
			</div>
		</div>
		
		<br>

		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Template:
				</div>
				<div class="card-body">
					<div class="form-group">
						<select name="template" id="template" class="form-control">
							<option value="fullwidth">Full Width</option>
							<option value="column3">3 Column Blog Template</option>
							<option value="column2">2 Column Blog Template</option>
							
						</select>
					</div>
				</div>
			</div>
		</div>


	</div>
</div>
