
<h1>Page List</h1>
<hr>	
<div class="col-md-12">	
	
	<a href="<?php echo base_url('page/new-page'); ?>"><button class="btn btn-primary">Add New</button></a>

	
</div>

<div class="col-md-12">
	
	<div class="card">
		<table class="table">	
			<thead>	
				<tr>
					<td>ID</td>
					<td>Title</td>					
					<td>Status</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>	
				<?php if (!empty($pages)): ?>
					<?php foreach ($pages as $page): ?>
						<tr>
							<td><?=$page->id?></td>
							<td><?=$page->page_title?></td>							
							<td><?=$page->page_status?></td>
							<td>
									<a href="<?php echo base_url('page/edit-page?num_page='.$page->id); ?>"><button class="btn btn-primary">Edit Page</button></a>
									<button class="btn btn-danger trash" data-ids = "<?= $page->id?>" data-vls = "trash">Move to Trash</button>
							</td>
						<?php endforeach ?>
					<?php endif ?>
				</tr>
			</tbody>
		</table>
	</div>
	
</div>