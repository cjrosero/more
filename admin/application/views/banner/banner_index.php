
<h1>Banner List</h1>
<hr>	


<div class="row">
	<div class="col-md-4 ">	
		<div class="card">
			<div class="card-header">
				Add New

			</div>
			<div class="card-body">
				<form action="<?=base_url('crud/upload_banner')?>"  method="post" enctype="multipart/form-data">	
					<label for="">File</label><input type="file" name = "uploads" class="form-control">
					<label for="">Page</label>
					<select name="page" id="" class="form-control">
						<option value="0">Default</option>
						<?php if (!empty($page)): ?>
							<?php foreach ($page as $bn): ?>
								<option value="<?=$bn->id?>"><?=$bn->page_title?></option>
								
							<?php endforeach ?>
						<?php endif ?>
					</select>
					<label for="">Title</label><input type="text" name = "title" class="form-control">
					<label for="">Status</label> 
					<select name="status" id="" class="form-control">
						<option value="draft">Draft</option>
						<option value="publish" >Publish</option>
					</select>
					<label for="">Desciption</label><textarea name="desc" id="desc" cols="30" rows="25"></textarea>
					<button class="btn btn-primary btn-block">Upload Banner</button>
				</form>
			</div>

		</div>
	</div>
	<div class="col-md-8">
		<div class="card">
			<table class="table">	
				<thead>	
					<tr>
						<td>ID</td>
						<td>Title</td>					
						<td>Image</td>					
						<td>Status</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>	
					<?php if (!empty($banners)): ?>
						<?php foreach ($banners as $banner): ?>
							<tr>
								<td><?=$banner->id?></td>
								<td><?=$banner->banner_title?></td>							
								<td><?=$banner->banner_title?></td>							
								<td><?=$banner->banner_status?></td>
								<td>
									<a href="<?php echo base_url('page/edit-page?num_page='.$banner->id); ?>"><button class="btn btn-primary">Edit Page</button></a>
								</td>
							<?php endforeach ?>
						<?php endif ?>
					</tr>
				</tbody>
			</table>
		</div>

	</div>



</div>