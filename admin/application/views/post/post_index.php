
<h1>Post List</h1>
<hr>	
<div class="col-md-12">	
	
	<a href="<?php echo base_url('post/new-post'); ?>"><button class="btn btn-primary">Add New</button></a>
	
</div>

<div class="col-md-12">
	
	<div class="card">
		<table class="table">	
			<thead>	
				<tr>
					<td>ID</td>
					<td>Title</td>					
					<td>Status</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>	
				<?php if (!empty($posts)): ?>
					<?php foreach ($posts as $post): ?>
						<tr>
							<td><?=$post->id?></td>
							<td><?=$post->page_title?></td>							
							<td><?=$post->page_status?></td>
							<td>
								<a href="<?php echo base_url('post/edit-post?num_page='.$post->id); ?>"><button class="btn btn-primary">Edit Page</button></a>
							</td>
						<?php endforeach ?>
					<?php endif ?>
				</tr>
			</tbody>
		</table>
	</div>
	
</div>