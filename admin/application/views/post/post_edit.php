
<h1>Edit Page</h1>
<div class="row">
	<div class="col md-9 ">

		<form action="page/save" method="POST">
			<div class="card-body">
				<div class="form-group">
					<label for="">Title:</label>
					<input type="text" class="form-control" name="title" id = "title" value="<?=$content->page_title?>">
					<label for="">Link:</label><input class="form-control" type="text" name = "slug"  id = "slug_input" value="<?=$content->page_slug?>">
					
				</div>
				<div class="form-group">
					<textarea name="desc" id="desc" cols="30" rows="25"><?=$content->page_description?></textarea>
				</div>
			</div>
		</form>

	</div>

	<div class="col-md-3">
		
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Page Option	
				</div>
				<div class="card-body">
					<div class="form-group">
						<label for="">Status:</label>
						<?php 
						if ($content->page_status == 'draft') {
							$draft = "selected";
							$publish = "";
						}else{
							$draft = "";
							$publish = "selected";
						}
						?>
						<select name="page_status" id="page_status" class="form-control">
							<option <?=$draft?> value="draft">Draft</option>
							<option <?=$publish?> value="publish">Publish</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Last Update:</label><span>test</span>
					</div>
					<div class="form-group">
						<button class="btn btn-primary btn-block" id = "save_page" data-type = "post" data-edit="<?=$content->id?>" >Save</button>
						
					</div>
				</div>
			</div>
		</div>
		
		<br>

		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Template:
				</div>
				<div class="card-body">
					<?php 
					if ($content->page_template == 'fullwidth') {
						$fullwidth = "selected";
						$column3 = "";
						$column2 = "";
					}else if($content->page_template == 'column3'){
						$fullwidth = "";
						$column3 = "selected";
						$column2 = "";
					}else if($content->page_template == 'column2'){
						$fullwidth = "";
						$column3 = "";
						$column2 = "selected";
					}
					?>
					<div class="form-group">
						<select name="template" id="template" class="form-control">
							<option value="fullwidth" <?=$fullwidth?>>Full Width</option>
							<option value="column2" <?=$column3?>>2 Column Blog Template</option>	
							<option value="column3"  <?=$column2?>>3 Column Blog Template</option>

						</select>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Category:
				</div>
				<div class="card-body">
					<?php if (!empty($categories)): 
					// var_dump($categories); echo "</br>"; echo "</br>";
					?>
						<?php foreach ($categories as $category): ?>						
						<div class="checkbox">
							<label for="checkbox">
								<input class="checkbox category" type="checkbox" <?=(!empty($caty[$category->id]['checked'])? $caty[$category->id]['checked'] : "" )?> name="cat[]" value="<?=$category->id?>">
								<?=$category->category_title?></label>	
							</div>
						<?php endforeach ?>
					<?php endif ?>

				</div>
			</div>
		</div>
		<br>
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Feature Image:
				</div>
				<div class="card-body">
					<div class="form-group">
						<?php if (!empty($content->page_feature)): ?>
							<img class = "img-thumbnail" src="<?=base_url('uploads/feature/'.$content->page_feature)?>" alt="">
							
						<?php endif ?>
						<form action="<?=base_url('crud/feature_image')?>"  method="post" enctype="multipart/form-data">
							<input type="file" class="form-control" name = "uploads"> 
							<input type="hidden" value="<?=$content->id?>" name = "post_id" id = "post_id">
							<button class="btn btn-primary btn-block">Set Feature Image</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
