
<h1>Add New post</h1>
<div class="row">
	<div class="col md-9 ">

		<form action="post/save" method="POST">
			<div class="card-body">
				<div class="form-group">
					<label for="">Title:</label>
					<input type="text" class="form-control" name="title" id = "title">
					<label for="">Link:</label><input class="form-control slug_input" type="text" name = "slug"  id = "slug_input" value="">
					
				</div>
				<div class="form-group">
					<textarea name="desc" id="desc" cols="30" rows="25"></textarea>
				</div>
			</div>
		</form>

	</div>

	<div class="col-md-3">
		
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					post Option	
				</div>
				<div class="card-body">
					<div class="form-group">
						<label for="">Status:</label>
						<select name="post_status" id="page_status" class="form-control">
							<option value="draft">Draft</option>
							<option value="publish">Publish</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Last Update:</label><span>test</span>
					</div>
					<div class="form-group">
						<button class="btn btn-primary btn-block" id = "save_post" data-type = "post">Save</button>
						
					</div>
				</div>
			</div>
		</div>
		
		<br>

		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Template:
				</div>
				<div class="card-body">
					<div class="form-group">
						<select name="template" id="template" class="form-control">
							<option value="fullwidth">Full Width</option>
							<option value="column3">3 Column Blog Template</option>
							<option value="column2">2 Column Blog Template</option>
							
						</select>
					</div>
				</div>
			</div>
		</div>

		<br>
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Category:
				</div>
				<div class="card-body">
					<?php if (!empty($categories)): ?>
						<?php foreach ($categories as $category): ?>						
							<div class="checkbox">
								<label for="checkbox">
									<input class="checkbox category" type="checkbox" name="cat[]" value="<?=$category->id?>">
									<?=$category->category_title?></label>	
								</div>
							<?php endforeach ?>
						<?php endif ?>

					</div>
				</div>
			</div>


		</div>
	</div>
