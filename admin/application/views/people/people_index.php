
<h1>People List</h1>
<hr>	


<div class="row">
	<div class="col-md-4 ">	
		<div class="card">
			<div class="card-header">
				Add New

			</div>
			<div class="card-body">
				<form action="<?=base_url('crud/insert_people')?>"  method="post" enctype="multipart/form-data">	
					<label for="">Image</label>
					<input type="file" name = "uploads" class="form-control">
					<label for="">Name</label>
					<input type="text" class="form-control" name = "p_name"  id = "title" >
					<label for="">Link</label>
					<input type="text" class="form-control slug_input" name = "p_slug"  id = "slug_input"  value="">
					<label for="">Position</label>
					<input type="text" class="form-control" name = "p_position">
					<label for="">Contact</label>
					<input type="text" name = "p_contact" class="form-control">
					<label for="">Address</label>
					<input type="text" class="form-control" name = "p_address">

					<label for="">Email</label>
					<input type="text" name = "p_email" class="form-control">
					<label for="">Desciption</label>
					<textarea name="desc" id="desc" cols="30" rows="25"></textarea>
					<button class="btn btn-primary btn-block">Save People</button>
				</form>
			</div>

		</div>
	</div>
	<div class="col-md-8">
		<div class="card">
			<table class="table">	
				<thead>	
					<tr>
						<td>ID</td>
						<td>Name</td>					
						<td>Image</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>	
					<?php if (!empty($peoples)): ?>
						<?php foreach ($peoples as $people): 
							$pid = $people->id;
							$pname = $people->p_name;
							$pimg = $people->p_image;


						?>
							<tr>
								<td><?=$pid;?></td>		
								<td><?=$pname?></td>		
								<td><?=$pimg?></td>		
								
								<td>
									<a href="<?php echo base_url('people/edit-people?num_page='.$people->id); ?>"><button class="btn btn-primary">Edit Page</button></a>
								</td>
							<?php endforeach ?>
						<?php endif ?>
					</tr>
				</tbody>
			</table>
		</div>

	</div>



</div>