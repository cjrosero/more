<h1>Menu Settings</h1>
<hr>



<div class="form-group">
	<a href = "<?=base_url('menu')?>" ><button class="btn btn-success">Menu</button></a>
	<a href = "<?=base_url('menu?data=menu_category')?>"><button  class="btn btn-primary">Menu Category</button></a>
</div>

<?php $category_settings_select; ?>

<?php if (empty($_GET['data'])): ?>
	<div class="row">
		
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					Menu Category
				</div>
				<div class="card-body">
					<form action="" method="GET" id = "menu_category_settings_form">
						<select name="menu_category_settings" id="menu_category_settings" class="form-control">

							<?php if (!empty($menu_categories)): ?>
								<?php foreach ($menu_categories as $mc_list): ?>
									<?php 
									$cat_select="";
									if (!empty($_GET['menu_category_settings'])) {									

										if ($_GET['menu_category_settings']==$mc_list->menu_category_id){
											$cat_select="selected";
										}	
									}?>
									<option <?=$cat_select?> value="<?=$mc_list->menu_category_id?>"><?=$mc_list->menu_category_title?></option>
								<?php endforeach ?>
							<?php endif ?>
						</select>
						<button class="btn btn-block btn-info">Select Settings</button>
					</form>

				</div>
			</div>


			<div class="card">
				<div class="card-header">
					Menu list
				</div>
				<div class="card-body">
					<div style="max-height: 400px;overflow: scroll;   overflow-x: hidden;">
						<button type="button" class="btn btn-info  btn-block" data-toggle="collapse" data-target="#page_link">Page</button>
						<div id="page_link" class="collapse">
							<ul class="list-group">
								<?php if (!empty($page_link)): ?>
									<?php foreach ($page_link as $pl): ?>
										<li class="list-group-item" id = "pl<?=$pl->id?>" data-idds = "<?=$pl->id?>"><?=$pl->page_title?> <a   href = "#" class = " right add_to_menu" data-page-title = "<?=$pl->page_title?>" data-ids = "<?=$pl->id?>" data-menu-type = "page" data-menu-category-id="<?=$category_settings_select?>">Add Page</a></li>

									<?php endforeach ?>
								<?php endif ?>

							</ul>
						</div>
					</div>

					<div style="max-height: 400px;overflow: scroll;   overflow-x: hidden;">
						<button type="button" class="btn btn-info  btn-block" data-toggle="collapse" data-target="#post_link">Post</button>
						<div id="post_link" class="collapse">
							<ul class="list-group">
								<?php if (!empty($post_link)): ?>
									<?php foreach ($post_link as $pol): ?>
										<li class="list-group-item" id = "pl<?=$pol->id?>"><?=$pol->page_title?> <a   href = "#" class = " right add_to_menu" data-menu-type = "post" data-page-title = "<?=$pol->page_title?>" data-ids = "<?=$pol->id?>" data-menu-category-id="<?=$category_settings_select?>">Add Menu</span></a></li>

									<?php endforeach ?>
								<?php endif ?>

							</ul>
						</div>
					</div>
					<div style="max-height: 400px;overflow: scroll;   overflow-x: hidden;">
						<button type="button" class="btn btn-info  btn-block" data-toggle="collapse" data-target="#cat_link">Category</button>
						<div id="cat_link" class="collapse">
							<ul class="list-group">
								<?php if (!empty($categories)): ?>
									<?php foreach ($categories as $cat): ?>
										<li class="list-group-item" id = "pl<?=$cat->id?>"><?=$cat->category_title?> <a   href = "#" class = " right add_to_menu" data-page-title = "<?=$cat->category_title?>" data-ids = "<?=$cat->id?>" data-menu-category-id="<?=$category_settings_select?>" data-menu-type = "category">Add Menu</span></a></li>

									<?php endforeach ?>
								<?php endif ?>

							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					Menu Selected
				</div>
				<div class="card-body">
					<!-- <ul class="list-group" id = "selected_menu">
						<?php if (!empty($link_select)):  ?>
							<?php foreach ($link_select as $ls): ?>
								<li id = "sl<?=$ls['mid']?>" class="list-group-item" >
									<?=$ls['page_title']?> </li>
								<?php endforeach ?>
							<?php endif ?>
						</ul>
 -->
						<table class="table">
							<thead>
								<th>Name</th>
								<th>Position</th>
								<th>Action</th>
							</thead>
							<tbody>
								<?php if (!empty($link_select)):  ?>
									<?php foreach ($link_select as $ls): ?>
										<tr id = "sl<?=$ls['mid']?>" >
											<td>	<?=$ls['page_title']?> </td>
											<td><input type="text" id = "sort_num" class="form-control" value="<?=$ls['menu_sort']?>"></td>
											<td>
												<button class="btn btn-success">Save</button> 
												<button class = "btn btn-danger remove_menu" href="#" data-myid = "<?=$ls['mid']?>" data-ids = "<?=$ls['id']?>" data-menu-category-id="<?=$category_settings_select?>" >Remove</button>
											</td>
										</tr>
									<?php endforeach ?>
								<?php endif ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col-md-4">

			</div>
			<div class="col-md-4">

			</div>
		</div>


	<?php else: ?>

		<div class="row">
			<div class="col-md-9">
				<div class="card">
					<div class="card-header">
						Menu Category List
					</div>
					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Menu Category Title</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php if (!empty($menu_categories)): ?>
									<?php foreach ($menu_categories as $mc): ?>
										<tr>
											<td><?=$mc->menu_category_id?></td>
											<td><?=$mc->menu_category_title?></td>
											<td><button>Remove</button></td>

										</tr>
									<?php endforeach ?>
								<?php endif ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div  class="col-md-3" >
				<form action="<?=base_url('crud/insert_menu_category')?>" id = "menu_category">
					<div class="card">
						<div class="card-header">
							New Menu Category
						</div>
						<div class="card-body">
							<div class="form-group ">
								<label for="">Menu Category Title</label>
								<input type="text" class="form-control" name = "menu_category_title" id = "menu_category_title">
								<button class="btn btn-primary btn-block f_submit" data-formid = "menu_category" id ="save" data-table = "menu_category">Save Menu Category</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

	<?php endif ?>