<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="">
  <meta name="author" content="">
  <title></title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('inc/vendor/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="<?php echo base_url('inc/vendor/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">

  <!-- Plugin CSS -->
  <link href="<?php echo base_url('inc/vendor/datatables/dataTables.bootstrap4.css');?>" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('inc/css/sb-admin.css');?>" rel="stylesheet">
  <link href="<?php echo base_url('inc/css/site.css');?>" rel="stylesheet">

  <link href="<?php echo base_url('inc/css/jquery-confirm.min.css');?>" rel="stylesheet">
  <link href="<?php echo base_url('inc/css/jquery-ui.min.css');?>" rel="stylesheet">

  <script type="text/javascript" src="<?=base_url('inc/ckeditor/ckeditor.js');?>"></script>
   <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"><link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

<script type="text/javascript">
  window.App = {
    "baseUrl" : "<?=base_url()?>",
    "removeDOM" : "",
  };

</script>


</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation -->
  <?php menu_nav($info);?>
  <div class="content-wrapper">

    <div class="container-fluid">

      <!-- Breadcrumbs -->
      <?php if (!empty($info['page_name'])): ?>
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?=$info['link']?>"><?=$info['controller_name']?></a>
          </li>
          <li class="breadcrumb-item active"><?=$info['page_name'];?></li>
        </ol>
      <?php endif ?>
      

      <h1><?php echo $info['page_name'] ?></h1>
      <?php $this->load->view($info['vfile']);?>

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- /.content-wrapper -->

  <footer class="sticky-footer">
    <div class="container">
      <div class="text-center">
        <small>Copyright &copy; Your Website 2017</small>
      </div>
    </div>
  </footer>

  <!-- Scroll to Top Button -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
  </a>

  <!-- Logout Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Select "Logout" below if you are ready to end your current session.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url('inc/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('inc/vendor/popper/popper.min.js')?>"></script>
  <script src="<?php echo base_url('inc/vendor/bootstrap/js/bootstrap.min.js')?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('inc/vendor/jquery-easing/jquery.easing.min.js')?>"></script>
  <script src="<?php echo base_url('inc/vendor/chart.js/Chart.min.js')?>"></script>
  <script src="<?php echo base_url('inc/vendor/datatables/jquery.dataTables.js')?>"></script>
  <script src="<?php echo base_url('inc/vendor/datatables/dataTables.bootstrap4.js')?>"></script>
  <script src="<?php echo base_url('inc/js/site.js')?>"></script>
  <script src="<?php echo base_url('inc/js/jquery-confirm.min.js')?>"></script>
  <script src="<?php echo base_url('inc/js/jquery-ui.js')?>"></script>


  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('inc/js/sb-admin.min.js')?>"></script>

<script>
      CKEDITOR.replace('desc',{
        filebrowserBrowseUrl : window.App.baseUrl+'/page/browse/',
        filebrowserUploadUrl : window.App.baseUrl+'/page/uploads/',

        filebrowserWindowWidth : '640',
        filebrowserWindowHeight : '480',
        on: {
          instanceReady: function() {
            this.dataProcessor.htmlFilter.addRules({
              elements: {
                img: function( el ) {
                  el.addClass( 'img-responsive' );
                }
              }
            });            
          }
        }
      });

    </script>

</body>

</html>
