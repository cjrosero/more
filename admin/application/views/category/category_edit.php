<div class="row">
	<div class="col-md-4">
		<div class="card">
			<div class="card-header">
				Add Category
			</div>
			<div class="card-body">
				<div class="form-group">
					<label for="">Title</label>
					<input type="text" class="form-control" id="category_title" value ="<?=$content->category_title?>">
				</div>
				<div class="form-group">					
					<label for="">Link</label>
					<input type="text" class="form-control" id="category_slug" value ="<?=$content->category_slug?>">
				</div>

				<div class="form-group">					
					<label for="">Description</label>
					<textarea name="" id="category_description" cols="30" rows="10" class="form-control"><?=$content->category_description?></textarea>
				</div>

				<div class="form-group">
					<button class="btn btn-primary btn-block" id = "category_save" data-ids = "<?=$content->id?>">Update Category</button>
				</div>

			</div>
		</div>

	</div>

	<div class="col-md-8">

		<div class="card">
			<table class="table">	
				<thead>	
					<tr>
						
						<td>Title</td>					
						<td>Description</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>	
					<?php if (!empty($categories)): ?>
						<?php foreach ($categories as $category): ?>
							<tr>
								
								<td><?=$category->category_title?></td>							
								<td><?=$category->category_description?></td>
								<td>
									<a href="<?php echo base_url('category/edit-category?num_page='.$category->id); ?>"><button class="btn btn-primary">Edit Page</button></a>
								</td>
							<?php endforeach ?>
						<?php endif ?>
					</tr>
				</tbody>
			</table>
		</div>

	</div>
</div>