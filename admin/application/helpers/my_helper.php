<?php 


if (! function_exists('response_json'))
{
	function response_json(array $data) 
	{
		$_CI =& get_instance();
		$_CI->output->set_content_type('application/json')->set_output(json_encode($data));
	}
}

if (! function_exists('ajax_req'))
{
	function ajax_req(){
		$_CI =& get_instance();
		if (!$_CI->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
	}
}

if (! function_exists('get_category_page'))
{
	function get_category_page($menu_id = '',$row=0)
	{
		$ci =& get_instance();
		$r = $ci->Main->raw("SELECT * FROM cms_category INNER JOIN cms_category_relationship ON cms_category.`id`= cms_category_relationship.`category_id` WHERE cms_category_relationship.`menu_id` = '$menu_id' ");

		if (!empty($r)) {
			return $r;
		}
		return null;
	}
}

if (! function_exists('main_head_nav'))
{
	function main_head_nav($menu_category_id = 0)
	{
		$ci =& get_instance();
		$menu =  $ci->Main->raw("SELECT * FROM cms_menu_relationship WHERE `menu_category_id` = '$menu_category_id' ORDER BY `menu_sort`=0, `menu_sort` ");
		$data = array();
		if (!empty($menu)) {

			foreach ($menu as $m) {
				$mid = $m->id;
				if ($m->menu_type == "category") {
					$c_menu = $ci->Main->select('*','cms_category',array('id'=>$m->menu_id),1);
					$menu_type = "category";
					$page_slug = $c_menu->category_slug;
					$page_title = $c_menu->category_title;
					$page_id = $c_menu->id;


				}else{
					$p_menu = $ci->Main->select('*','cms_post',array('id'=>$m->menu_id),1);
					$menu_type = $p_menu->page_type;
					$page_slug = $p_menu->page_slug;
					$page_title = $p_menu->page_title;
					$page_id = $p_menu->id;


				}
				$data[] = array(
					'menu_type' => $menu_type,
					'page_slug' => $page_slug,
					'page_title' => $page_title,
					'id' => $page_id,
					'mid' => $mid,
					'menu_sort' => $m->menu_sort,
				);
			}
		}

		// echo "<pre>";var_dump($data);die();
		return $data;
	}
}