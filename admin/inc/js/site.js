$(document).ready(function(){
	$( "#title" ).keyup(function(e) {
		console.log(e)
		var val = $(this).val();
		val = val.toLowerCase()
		val = val.replace(/\s+/g, '-').toLowerCase();
		
		$("#slug").text(val);
		$(".slug_input").val(val);  

	});

	$(document).on('click','#save_page,#save_post',function(e){
		e.preventDefault();
		var title = $("#title").val(); 
		var slug_input = $("#slug_input").val();
		var page_post = CKEDITOR.instances['desc'].getData();
		var page_status = $('#page_status').val();template
		var template = $('#template').val();
		var edit = $(this).data('edit');
		var cat = $('.category');
		var tags = [];

		$.each( cat, function( key, value ) {
			if (value.checked) {
				tags.push(value.value);
			}
			console.log(value.checked );
		});

		console.log(tags);
		



		var table = "cms_post";

		if ($(this).data('type')=="post"){
			type = 'post';
		}else{
			type = 'page';

		}

		$.ajax( {
			type: "POST",
			url: window.App.baseUrl + 'crud/insert_page',
			data:{"tags":tags,"page_template":template,"edit":edit,"page_title":title, "page_slug":slug_input, "page_description":page_post,"table":table,'page_status':page_status,"type":type},
			dataType: 'json',
			method: 'POST',
			success: function( e ) {
				if (e.success) {
					$.confirm({
						title: 'Success',
						content: e.msg,
						type: 'green',
						typeAnimated: true,
						buttons: {
							tryAgain: {
								text: 'Ok',
								btnClass: 'btn-success',
								action: function(){
								}
							},
							close: function () {
							}
						}
					});
					if (!e.updated) {
					setTimeout(function(){ window.location =  window.App.baseUrl+e.type  }, 2000);	
					}
					
				}else{
					$.confirm({
						title: 'Error',
						content: e.msg,
						type: 'red',
						typeAnimated: true,
						buttons: {
							tryAgain: {
								text: 'Ok',
								btnClass: 'btn-success',
								action: function(){
									$('#pl'+e.menu_id).remove();
									$('#selected_menu').append('<li class="list-group-item">'+e.page_title+'</li>');
								}
							},
							
						}
					});
				}
			}
		} );


	});

	
	// $(document).on('change','#menu_category_settings',function(e){
	// 	$('#menu_category_settings_form').submit();
	// });

	$(document).on('click','.add_to_menu',function(e){
		e.preventDefault();
		var id = $(this).data('ids');
		var page_title = $(this).data('page-title');
		var menu_category_id = $(this).data('menu-category-id');
		var menu_type = $(this).data('menu-type');
		
		$.ajax( {
			type: "POST",			
			url: window.App.baseUrl + "crud/insert_relation_menu",
			data: {"id":id,"menu_category_id":menu_category_id,"page_title":page_title,"menu_type":menu_type},
			dataType: 'json',
			method: 'POST',
			success: function( e ) {
				if (e.success) {
					$.confirm({
						title: 'Success',
						content: e.msg,
						type: 'green',
						typeAnimated: true,
						buttons: {
							tryAgain: {
								text: 'Ok',
								btnClass: 'btn-success',
								action: function(){
									$('#pl'+e.menu_id).remove();
									$('#selected_menu').append('<li class="list-group-item">'+e.page_title+'</li>');
								}
							},
							
						}
					});
				}else{

				}
			}
		} );
	});

	
	$(document).on('click','.remove_menu',function(e){
		e.preventDefault();
		var id = $(this).data('ids');		
		var myid = $(this).data('myid');		
		var menu_category_id = $(this).data('menu-category-id');
		
		$.ajax( {
			type: "POST",			
			url: window.App.baseUrl + "crud/remove_selected_menu",
			data: {"id":id,"menu_category_id":menu_category_id,"myid":myid},
			dataType: 'json',
			method: 'POST',
			success: function( e ) {
				if (e.success) {
					$.confirm({
						title: 'Success',
						content: e.msg,
						type: 'green',
						typeAnimated: true,
						buttons: {
							tryAgain: {
								text: 'Ok',
								btnClass: 'btn-success',
								action: function(){
									$('#sl'+e.myid).remove();
									
								}
							},
							
						}
					});
				}else{

				}
			}
		} );
	});

	


	$(document).on('click','#category_save',function(e){
		e.preventDefault();
		var category_title = $('#category_title').val();
		var category_slug = $('#category_slug').val();
		var category_description = $('#category_description').val();
		var id = $(this).data('ids');


		$.ajax( {
			type: "POST",			
			url: window.App.baseUrl + "crud/category_insert",
			data: {"category_title":category_title, "category_slug":category_slug, "category_description":category_description,"id":id} ,
			dataType: 'json',
			method: 'POST',
			success: function( e ) {
				if (e.success) {
					$.dialog({
						title: 'Success',
						content: e.msg,
						type: 'green',
						typeAnimated: true,
						buttons: {
							tryAgain: {
								text: 'Ok',
								btnClass: 'btn-success',
								action: function(){
								}
							},
							close: function () {
							}
						}
					});
				}else{
					$.dialog({
						title: 'Error',
						content: e.msg,
						type: 'red',
						typeAnimated: true,
						buttons: {
							tryAgain: {
								text: 'Ok',
								btnClass: 'btn-success',
								action: function(){
								}
							},
							close: function () {
							}
						}
					});
				}
			}
		} );

	});

	$(document).on('click','.f_submit',function(e){
		e.preventDefault()
		

		var form_id = $(this).data('formid');
		var form = $('#'+form_id);

		$.ajax( {
			type: "POST",			
			url: form.attr( 'action' ),
			data: form.serialize(),
			dataType: 'json',
			method: 'POST',
			success: function( e ) {
				if (e.success) {
					$.confirm({
						title: 'Success',
						content: e.msg,
						type: 'green',
						typeAnimated: true,
						buttons: {
							tryAgain: {
								text: 'Ok',
								btnClass: 'btn-success',
								action: function(){
								}
							},
							close: function () {
							}
						}
					});
				}else{

				}
			}
		} );
	});



	$("#selected_menu").sortable({
		opacity: 0.6, 
		cursor: 'move', 
		update: function(event,ui) {
			var id = ui.item.attr("id");
			console.log(id);
		}
	});



});