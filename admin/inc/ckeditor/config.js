/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

 CKEDITOR.editorConfig = function( config ) {
 	config.toolbarGroups = [
 	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
 	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
 	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
 	{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
 	{ name: 'forms', groups: [ 'forms' ] },
 	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
 	{ name: 'forms', groups: [ 'forms' ] },		
 	{ name: 'links', groups: [ 'links' ] },
 	{ name: 'styles', groups: [ 'styles' ] },
 	{ name: 'insert', groups: [ 'insert' ] },
 	{ name: 'colors', groups: [ 'colors' ] },
 	{ name: 'tools', groups: [ 'tools' ] },
 	{ name: 'others', groups: [ 'others' ] },
 	{ name: 'about', groups: [ 'about' ] }
 	];
 	config.autoParagraph = false;
 	config.enterMode = CKEDITOR.ENTER_BR;
 	config.shiftEnterMode = CKEDITOR.ENTER_BR;

 	config.extraPlugins = 'uploadimage';
	// config.uploadUrl = '/News/upload_image';
	config.filebrowserImageBrowseUrl = window.App.baseUrl +'/admin/browse';
	config.image_previewText = CKEDITOR.tools.repeat(' ', 100 );
	 config.height = '400px';
	config.removeButtons = '';
};