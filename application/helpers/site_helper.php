<?php 


/**
 * Function Name
 *
 * Function description
 *
 * @access	public
 * @param	type	name
 * @return	type	
 */

if (! function_exists('main_top_nav'))
{
	function main_top_nav($param = '')
	{
		$ci =& get_instance();
		return $ci->Main->raw("SELECT cms_post.*,cms_menu_relationship.menu_id as m_id FROM cms_post
			INNER JOIN cms_menu_relationship
			ON cms_post.`id` = cms_menu_relationship.`menu_id`
			WHERE cms_menu_relationship.`menu_category_id` = '1'");
	}
}

if (! function_exists('get_category_page'))
{
	function get_category_page($menu_id = '',$row=0)
	{
		$ci =& get_instance();
		if (!empty($menu_id)) {
			$where = "WHERE cms_category_relationship.`menu_id` = '".$menu_id."'";
		}
		$r = $ci->Main->raw("SELECT * FROM cms_category INNER JOIN cms_category_relationship ON cms_category.`id`= cms_category_relationship.`category_id` $where ",$row);

		if (!empty($r)) {
			return $r;
		}
		return null;
	}
}

if (! function_exists('main_head_nav'))
{
	function main_head_nav($menu_category_id = 0)
	{
		$ci =& get_instance();
		$menu =  $ci->Main->raw("SELECT * FROM cms_menu_relationship WHERE `menu_category_id` = '$menu_category_id'");

		foreach ($menu as $m) {

			if ($m->menu_type == "category") {
				$c_menu = $ci->Main->select('*','cms_category',array('id'=>$m->menu_id),1);
				$menu_type = "category";
				$page_slug = $c_menu->category_slug;
				$page_title = $c_menu->category_title;
			}else{
				$p_menu = $ci->Main->select('*','cms_post',array('id'=>$m->menu_id),1);
				$menu_type = $p_menu->page_type;
				$page_slug = $p_menu->page_slug;
				$page_title = $p_menu->page_title;
				
			}
			$data[] = array(
				'menu_type' => $menu_type,
				'page_slug' => $page_slug,
				'page_title' => $page_title,
				'menu_parent' => $m->menu_parent,
				'menu_id' => $m->menu_id,
			);
		}
		// echo "<pre>";var_dump($data);die();
		return $data;
	}
}

if (! function_exists('get_sub_menu'))
{
	function get_sub_menu($parent_id = 0)
	{
		$ci =& get_instance();
		$menu =  $ci->Main->raw("SELECT * FROM cms_menu_relationship WHERE `menu_parent` = '$parent_id'");
		$data = array();
		foreach ($menu as $m) {

			if ($m->menu_type == "category") {
				$c_menu = $ci->Main->select('*','cms_category',array('id'=>$m->menu_id),1);
				$menu_type = "category";
				$page_slug = $c_menu->category_slug;
				$page_title = $c_menu->category_title;
			}else{
				$p_menu = $ci->Main->select('*','cms_post',array('id'=>$m->menu_id),1);
				$menu_type = $p_menu->page_type;
				$page_slug = $p_menu->page_slug;
				$page_title = $p_menu->page_title;
				
			}
			$data[] = array(
				'menu_type' => $menu_type,
				'page_slug' => $page_slug,
				'page_title' => $page_title,
				'menu_parent' => $m->menu_parent,
			);
		}
		// echo "<pre>";var_dump($data);die();
		return $data;
	}
}
if (! function_exists('banner'))
{
	function banner_func($slug="")
	{
		$ci =& get_instance();
		$banner = $ci->Main->raw("SELECT cms_banner.* FROM cms_banner INNER JOIN cms_post ON cms_post.id= cms_banner.post_id WHERE cms_post.page_slug = '$slug' ");
		if (empty($banner)) {
			$banner = $ci->Main->raw("SELECT * FROM cms_banner WHERE `post_id` = 0 AND `banner_status` = 'publish' ");
			
		}

		return $banner;

	}
}


if (! function_exists('main_footer_nav'))
{
	function main_footer_nav($param = '')
	{
		$ci =& get_instance();
		return $ci->Main->raw("SELECT cms_post.*,cms_menu_relationship.menu_id as m_id FROM cms_post
			INNER JOIN cms_menu_relationship
			ON cms_post.`id` = cms_menu_relationship.`menu_id`
			WHERE cms_menu_relationship.`menu_category_id` = '3'");
	}
}

if (! function_exists('print_die'))
{
	function print_die($data = array())
	{
		echo "<pre>";
		var_dump($data);
	}
}