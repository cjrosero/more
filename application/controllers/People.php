<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class People extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Main');
	}

	public function index()
	{
		$slug = $this->uri->segment(2);
		$data['slug'] = $slug;
		
		$data['banner'] = banner_func($slug);
		if (empty($slug)) {
			redirect('/p/people');
		}

		$data['content'] = $this->Main->select('*','cms_people',array('p_slug' => $slug),1);
		if (empty($data['content'])) {
			redirect('/p/people');
			
		}
		$vfile = 'single_people';

		$data['info'] = array(
			'vfile' => $vfile, );
		$this->load->view('template', $data);
	}

}

/* End of file People.php */
/* Location: ./application/controllers/People.php */