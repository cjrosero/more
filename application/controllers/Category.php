<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function index(){
		$slug = $this->uri->segment(2);
		
		$data['tag_content'] = array();
		$tc = "0,";
		if (empty($slug)) {
			$slug = "";
		}

		$vfile = 'template/blog_template';
		$data['banner'] = banner_func($slug);
		$content = $this->Main->raw("SELECT * FROM  cms_post WHERE page_slug = '$slug' ",1);

		if (empty($content)) {
			// show_404();
		}else{
			$data['active'] = $content->id;
			$data['tags'] = get_category_page($content->id);
			$data['ptype'] = true;
			
		}


		$category_id = $this->Main->raw("SELECT id FROM cms_category WHERE category_slug = '$slug' ",true);
		if (!empty($category_id)) {
			$get_categories = $this->Main->raw("SELECT menu_id FROM cms_category_relationship WHERE category_id = '$category_id->id' ");
		}else{
			$category_id = $this->Main->raw("SELECT * FROM cms_category_relationship WHERE menu_id = '$content->id' ",true);
			if (!empty($category_id)) {
				$get_categories = $this->Main->raw("SELECT menu_id FROM cms_category_relationship WHERE category_id = '$category_id->category_id' ");
			}

		}

		if (!empty($get_categories)) {
			foreach ($get_categories as $gc ) {
				$tc .= $gc->menu_id.",";
			}
		}
		$tc = rtrim($tc, ',');

		$content = $this->Main->raw("SELECT * FROM cms_post WHERE id IN ($tc) ");
		$data['content'] = $content;
		
		$data['info'] = array(
			'vfile' => $vfile, );
		
		$this->load->view('template', $data);
		
	}
}
