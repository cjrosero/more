<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Main');
	}

	public function index()
	{
		$slug = $this->uri->segment(1);
		if (empty($slug)) {
			$slug = "home";
		}

		$data['info'] = array(
			'vfile' => 'home', );
		$data['banner'] = $this->Main->raw("SELECT cms_banner.* FROM cms_banner INNER JOIN cms_post ON cms_post.id= cms_banner.post_id WHERE cms_post.page_slug = '$slug' ");
		if (empty($data['banner'])) {
		$data['banner'] = $this->Main->raw("SELECT * FROM cms_banner WHERE `post_id` = 0 ");
			
		}
		$data['new_post'] = $this->Main->raw("SELECT * FROM cms_post WHERE page_type = 'post' and page_status = 'publish' LIMIT 2 ");
		
	
		$this->load->view('template', $data);

	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */