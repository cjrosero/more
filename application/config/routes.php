<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['p/(:any)'] = 'Page';
$route['c/(:any)'] = 'Category';
$route['people/(:any)'] = 'People';
$route['translate_uri_dashes'] = FALSE;
