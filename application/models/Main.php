<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Model {



	public function __construct()
	{
		parent::__construct();
		
	}

	public function insert($table,$data){
		return $this->db->insert($table,$data);
	}

	public function select($select,$table,$condition=array(),$row=0,$sort=array(),$in=array()){
		$this->db->select($select);
		$this->db->from($table);
		if (!empty($condition)) {
			$this->db->where($condition);
		}

		if (!empty($sort)) {
			$this->db->order_by($sort['col'],$sort['sort']);
		}
		if (!empty($in)) {
			$this->db->where_not_in($in['menu_id'], $in['data']);
		}

		$query = $this->db->get();
		if ($query->num_rows()) {
			if ($row) {
				return $query->row();
			}else{
				return $query->result();
			}
			
		}
		
	}

	public function delete($table,$data){
		return $this->db->delete($table,$data);
	}

	public function raw($query,$type = 0){
		$query = $this->db->query($query);
		if ($type) {
			return $query->row();
			
		}
		return $query->result();
	}

}

/* End of file Main.php */
/* Location: ./application/models/Main.php */