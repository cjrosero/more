<div class="breadcrumbs">
	<div class="container">
		<ul>
			<li><a href="<?=base_url()?>">Home</a></li>
			<li><span><i class="fa fa-long-arrow-right"></i></span></li>
			<li><a href=""><?=$content->p_name?></a> </li>
		</ul>
	</div>
</div>

<div style="background: #ffffff">
	<div class="container main-content">
		<div class="row">
			<div class="col-md-8">
				<div class="island person-profile">
					<div class="row head-bio">
						<div class="col-md-9">
							<h1><?=$content->p_name?></h1>
							<h2><?=$content->p_position?></h2>
							<article class="person-contact-details">

								<div class="person-contact-detail">
									<a class="person-contact-detail-info" href="<?=$content->p_contact?>"><?=$content->p_contact?></a>
								</div>

								<div class="person-contact-detail person-contact-detail-email">
									<a class="person-contact-detail-info" href="mailto:<?=$content->p_email?>"><?=$content->p_email?></a>
								</div>

							</article>

						</div>
						<div class="col-md-3">
							<div class="person-profile-photo">
								<img src="<?=base_url('uploads/people/'.$content->p_image)?>" alt="" title="Ahmed Abbasi">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6"></div>
						<div class="col-md-6"></div>
						<article class="person-bio">
							<?=$content->p_desc?>
						</article>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<aside class="sidebar">
					<div id="p_lt_ctl08_pageplaceholder_p_lt_dataDiv" class="block block--person">
						<header class="block-head block-head--icon">
							<h1 class="block-title">
								Associated firms
							</h1>
						</header>
						<section class="block-body">
							<div class="contact-sidebar">

								<div class="contact-card">



									<div class="contact-piece">
										<h4 class="contact-title">Mendoza Querido &amp; Co. CPAs</h4>
										<p class="contact-data">19 th Floor Salcedo Towers, H.V. Dela Costa St. <br>Salcedo Village, Makati City</p>
									</div>
									
								</div>
								<div class="contact-sidebar">                      
									<div class="contact-card">
										<div class="contact-piece">

										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</aside>
			</div>
		</div>
	</div>
</div>