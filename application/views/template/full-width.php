<div class="breadcrumbs">
	<div class="container">
		<ul>
			<li><a href="">Home</a></li>
			<li><span><i class="fa fa-long-arrow-right"></i></span></li>
			<li><a href="">News & Views</a> </li>
		</ul>
	</div>
</div>

<div class="container main-content">
	 <div class="loc_intro">
					<div>
<?php 

echo $content->page_description;

 ?>
 </div>
				</div>


						
					
<?php if (!empty($slug == 'people')):  ?>
<div class="people-div">
<?php $this->load->view('people'); ?>
</div>
<?php endif ?>
</div>