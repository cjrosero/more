
	 <section class="page-breadcrumb">
        <div class="container-fluid-width"><a href="<?=base_url()?>">Home </a><i class="fa fa-long-arrow-right"></i><span>Lorem</span></div>
      </section>

<section class="content-section main-content">
	<div class="container-fluid-width">
		<div class="content-grid inner-grid row">
			<h2 class="col-12">News & Views</h2>
			<div class="content-mid col-8 tab-col-12">
				<div class="search-box tablet-only">
					<input type="text" placeholder="Search news">
					<button class="fa fa-search"></button>
				</div>
				<div class="news-list">

				
	<?php if (!empty($content)): ?>
		<?php foreach ($content as $con): ?>

			<div class="news-item">
						<a href="<?=base_url('p/'.$con->page_slug)?>" class="img-container"><img src="<?=base_url('admin/uploads/feature/'.$con->page_feature)?>"></a>
						<h2><a href="#"><?=$con->page_title?></a><span> <i class="fa fa-clock-o"></i>13 October 2017</span></h2>
						<?=$con->page_description?>
						<footer><a href="<?=base_url('p/'.$con->page_slug)?>" class="btn">View more guides</a></footer>
					</div>
			
			<!-- <div class="row">
				<div class="col-md-8 news_details">
					<section class="post-content news_details_cont">
						<a href="<?=base_url('p/'.$con->page_slug)?>"><h1><?=$con->page_title?></h1> 
							<img class = "img img-thumbnail" src="<?=base_url('admin/uploads/feature/'.$con->page_feature)?>" alt="" class="news_img">

						</a>
						<div class="news_date">
							<span><i class="fa fa-clock-o"></i></span>6 July 2017
							<a class="btn btn-small share_btn pull-right" href="./news/news_details" target="_blank">Share<span><i class="fa fa-share-alt"></i></span></a>
						</div>
						<p class="post-description news_full_content">
							<?=$con->page_description?>
						</p>

						<div class="tags_div">
							<?php if (!empty($ptype)): ?>

								<a class="btn btn-small tag_btn" href="<?=base_url('p/'.$con->page_slug)?>" target="_blank">Read More.</a>
							<?php else: ?>
								<span><i class="fa fa-tags"></i></span>
								<?php 
								$mytag = get_category_page($con->id);
								?>
								<?php if (!empty($mytag)):?>
									<?php foreach ($mytag as $tag): ?>
										<a class="btn btn-small tag_btn" href="<?=base_url('c/'.$tag->category_slug)?>" target="_blank"><?=$tag->category_title?></a>
									<?php endforeach ?>
								<?php endif ?>
							<?php endif ?>


							
						</div>
					</section>
				</div>
				<div class="col-md-4">
					<div class="input-group input-group-sm search_bar">
						<input type="text" class="form-control" id="search_news" placeholder="Search news" aria-describedby="sizing-addon3">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button" id="search_news_btn"><span><i class="fa fa-search" aria-hidden="true"></i></span></button>
						</span>
					</div>
				</div>

			</div> -->
		<?php endforeach ?>

	<?php else: ?>
		<div class="text-center">No news added</div>
	<?php endif ?>

</div>
				<div class="pagination"><a href="#" class="fa fa-chevron-left"></a><a href="#" class="active">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a><a href="#" class="fa fa-chevron-right"></a><span>Showing results <b>1</b> to <b>20</b> of <b>81</b></span></div>
			</div>
		</div>
	</div>
</section>
