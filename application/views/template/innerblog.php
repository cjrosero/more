   <section class="content-section main-content">
        <div class="container-fluid-width">
          <div class="content-grid inner-grid row">
            <h2 class="col-12">News & Views</h2>
            <div class="content-mid col-8">
              <div class="news-item">
                <h1><?=$content->page_title?></h1>
                <a href="#" class="img-container"><img src="img/content/c1.jpg"></a>
                <h2><span> <i class="fa fa-clock-o"></i>13 October 2017</span><a href="#" class="btn">
                     
                    Share<i class="fa fa-share-alt"></i></a></h2>

                    <!-- Description -->
                    <?=$content->page_description?>
                    <!-- End Description -->

                <footer class="meta-tags">
                  <i class="fa fa-tag"></i>
                  
                  <?php 

                $mytag = get_category_page($content->id);
                ?>
                <?php if (!empty($mytag)):?>
                  <?php foreach ($mytag as $tag): ?>
                    <a href="<?=base_url('c/'.$tag->category_slug)?>" class="btn"><?=$tag->category_title?></a>
                   
                  <?php endforeach ?>
                <?php endif ?>
                 </footer>
              </div>
            </div>
            <div class="content-right col-4">
              <div class="search-box">
                <input type="text" placeholder="Search news">
                <button class="fa fa-search"></button>
              </div>
            </div>
          </div>
        </div>
      </section>