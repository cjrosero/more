

	 <section class="page-breadcrumb">
        <div class="container-fluid-width"><a href="<?=base_url()?>">Home </a><i class="fa fa-long-arrow-right"></i><span><?=$content->page_title?></span></div>
      </section>


	      <section class="content-section main-content">
        <div class="container-fluid-width">
          <div class="content-grid inner-grid row">
            <div class="col-12 tablet-only content-left-control">
            	<a href="#" class="btn content-left-toggle">View All Services </a>
            </div>
            <div class="content-left col-3 tab-col-12">
            	<!-- <a href="#"> <span>Lorem Ipsum</span></a> -->
            	
            		<?php if (!empty($tag_content)): ?>
							<?php foreach ($tag_content as $r): 
							
							
							if ($r->id == $active) {
								$active_link = "active_blog";
							}else{
								$active_link = "";
							}

							?>
							<a href="<?=base_url('p/'.$r->page_slug)?>"><?=$r->page_title?></a>
											
								
							<?php endforeach ?>
						<?php endif ?>
            	
            </div>
            <div class="content-mid col-6 tab-col-12">
              <h2><?=$content->page_title?></h2>
              <?=$content->page_description?>
            </div>
            <!-- WIDGET HERE -->
            <div class="content-right col-3 tab-col-12">
              <div class="home-item"><a href="#" class="img-container"><img src="img/content/w16.jpg"></a>
                <header>Related publication</header>
                <h2><a href="#">Bespoke solutions for international businesses</a></h2>
                <p>Through the development of strong, long-term relationships we will gain an understanding into what makes you successful.</p>
                <footer><a href="#" class="btn">View more guides</a></footer>
              </div>
            </div>
          </div>
        </div>
      </section>