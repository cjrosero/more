
      <section class="content-section main-content">
        <div class="container-fluid-width">
          <div class="content-grid row home-grid">
            <div class="content-left col-8 tab-col-12">
              <div class="col-12">
                <div class="home-featured home-item clearfix"><a href="#" class="col-6 img-container mob-col-12"><img src="<?=base_url()?>assets/img/content/w1.jpg"></a>
                  <div class="col-6 home-desc mob-col-12">
                    <header>News</header>
                    <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">New global CEO appointed</a></h2>
                    <p>We’re delighted to announce that Anton Colella has been appointed as Moore Stephens International’s new global Chief Executive Officer following a worldwide search.</p>
                    <p>Anton Colella is currently global Chief Executive of the Institute of Chartered Accountants of Scotland (ICAS).</p>
                    <footer><a href="#" class="btn">Read more</a></footer>
                  </div>
                </div>
              </div>
              <div class="col-12">   
                <div class="col-6 mob-col-12">
                  <div class="col-12">
                    <div class="home-item"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w2.png"></a>
                      <header>Guide</header>
                      <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">Doing business in Lithuania</a></h2>
                      <footer><a href="#" class="btn">Read more</a></footer>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="home-item light-blue"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w3.png"></a>
                      <h2><a href="#">How Moore Stephens can help</a></h2>
                      <p>Moore Stephens International Limited is a global network of independent professional service firms. With our headquarters in London, our roots go back over 100 years. Throughout our history, we’ve provided solutions to meet our clients’ national and international objectives.</p>
                      <footer><a href="#" class="btn">Read more</a></footer>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="home-item bold-blue"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w4.png"></a>
                      <header>Brochure</header>
                      <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">Moore Stephens Latin America</a></h2>
                      <p>With a growing presence in Latin America, Moore Stephens was established there in 1910 and today has presence in 17 countries, with 36 offices and 1,400 professionals.</p>
                      <footer><a href="#" class="btn">Download publication</a></footer>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="home-item"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w5.png"></a>
                      <header>Publication</header>
                      <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">Strength amidst uncertainty</a></h2>
                      <p>The owner managed business view in 2017</p>
                      <footer><a href="#" class="btn">Download publication</a></footer>
                    </div>
                  </div>
                </div>
                <div class="col-6 mob-col-12">
                  <div class="col-12">
                    <div class="home-item pastel-green"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w6.jpg"></a>
                      <header>News</header>
                      <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">CICPA Secretary General Met Delegates from Moore Stephens International</a></h2>
                      <p>On May 5, 2017, Dr. Chen Yugui, Secretary General of CICPA met with Mr. Mike Hathorn, COO of Moore Stephens International.</p>
                      <footer><a href="#" class="btn">Read more</a></footer>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="home-item pastel-blue"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w7.png"></a>
                      <header>Guide</header>
                      <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">Doing Business in Denmark</a></h2>
                      <footer><a href="#" class="btn">View guide</a></footer>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="home-item bold-green"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w8.png"></a>
                      <header>Guide</header>
                      <h2><a href="#">Doing business in Mexico 2017</a></h2>
                      <p>This guide is designed to give an insight into doing business in Mexico jointly with relevant background information, which could be useful for organisations considering establishing a business in this country, either as a separate entity, as a subsidiary of an existing foreign company, or anyone who is considering to work or live in Mexico.</p>
                      <footer><a href="#" class="btn">Download the guide</a></footer>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="home-item pastel-blue"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w9.png"></a>
                      <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">Moore Stephens - US-UK cross border transactions</a></h2>
                      <footer><a href="#" class="btn">View brochure</a></footer>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="home-item pastel-green">
                      <header>Guides</header>
                      <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">More "Doing Business in" guides</a></h2>
                      <p>The Moore Stephens Doing Business In series of guides have been prepared by Moore Stephens member firms in the relevant country in order to provide general information for persons contemplating doing business with or in the country concerned...</p>
                      <footer><a href="#" class="btn">View more guides</a></footer>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-right col-4 tab-col-12">
              <div class="col-12 tab-col-6 mob-col-12">
                <div class="home-item"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w10.jpg"></a>
                  <header>News</header>
                  <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">Armanino Managing Partner Andy Armanino Named a Most Admired Peer by INSIDE Public Accounting</a></h2>
                  <p>Moore Stephens member firm, Armanino LLP , one of the top 25 largest accounting and business consulting firms in the U.S., has announced that its managing partner, Andy Armanino, was named to the INSIDE Public Accounting (IPA) Most Admired Peer list for the sixth time in the past seven years.</p>
                  <footer><a href="#" class="btn">Read more</a></footer>
                </div>
              </div>
              <div class="col-12 tab-col-6 mob-col-12">
                <div class="home-item light-blue"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w11.png"></a>
                  <header>Guide</header>
                  <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">Doing business in Switzerland</a></h2>
                  <footer><a href="#" class="btn">Read more</a></footer>
                </div>
              </div>
              <div class="col-12 tab-col-6 mob-col-12">
                <div class="home-item"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w12.jpg"></a>
                  <header>Publication</header>
                  <h2><a href="#">Bigger and better in Madrid</a></h2>
                  <p>Moore Stephens Madrid is pleased to announce the move to a new office for the firm, needed due to the growth over the last few years.</p>
                  <footer><a href="#" class="btn">Read more</a></footer>
                </div>
              </div>
              <div class="col-12 tab-col-6 mob-col-12">
                <div class="home-item bold-blue"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w13.png"></a>
                  <header>News</header>
                  <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">Doing business in the UK</a></h2>
                  <p>This brochure is focused for overseas companies who are currently operating in the UK, or are looking to establish themselves in the UK and includes our top tips and considerations.</p>
                  <footer><a href="#" class="btn">Read more</a></footer>
                </div>
              </div>
              <div class="col-12 tab-col-6 mob-col-12">
                <div class="home-item"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w14.png"></a>
                  <header>Publication</header>
                  <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">Moore Stephens - Asia Pacific-UK cross border transactions</a></h2>
                  <p>Moore Stephens Madrid is pleased to announce the move to a new office for the firm, needed due to the growth over the last few years.</p>
                  <footer><a href="#" class="btn">Read more</a></footer>
                </div>
              </div>
              <div class="col-12 tab-col-6 mob-col-12">
                <div class="home-item light-blue"><a href="#" class="img-container"><img src="<?=base_url()?>assets/img/content/w15.png"></a>
                  <h2 class="has-widget"><i class="fa fa-file-text-o"></i><a href="#">Moore Stephens - Japan-UK cross border transactions</a></h2>
                  <footer><a href="#" class="btn">Read more</a></footer>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>