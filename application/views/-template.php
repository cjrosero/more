<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<meta description = "<?="description"?>">
	<link rel="stylesheet" href="<?= base_url('inc/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('inc/css/site.css') ?>">
	<link rel="stylesheet" href="<?= base_url('inc/css/owl.carousel.css') ?>">
	<link rel="stylesheet" href="<?= base_url('inc/css/font-awesome.min.css') ?>">
	<script src="<?=base_url('inc/js/jquery.min.js')?>"></script>
	<script src="<?=base_url('inc/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('inc/js/owl.carousel.js')?>"></script>
	<script src="<?=base_url('inc/js/site.js')?>"></script>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
</head>
<body> 
	<header>
		<div class="container">
			<div class="row">

				<div class="col-md-6">
					<div class="logo_cont">
						<a href="<?=base_url()?>"><img src="<?= base_url('inc/img/logo.png')?>" alt="Logo"></a>
						<div>
							International
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="side_links_cont text-right">
						<div class="about_cont">
							<ul>
								<?php $top_menu = main_top_nav(); ?>
								<?php if (!empty($top_menu)): ?>
									<?php foreach ($top_menu as $tm):
									if ($tm->menu_type == "category") {
										$mtype = "c";
									}else{
										$mtype = "p";

									}
									?>

									<li><a href="<?=base_url($mtype.'/'.$tm->page_slug)?>"><?=$tm->page_title?></a></li>

								<?php endforeach ?>
							<?php endif ?>

						</ul>
					</div>
					<div class="logo_cont">
						<a href="<?=base_url()?>"><img src="<?= base_url('inc/img/logo.png')?>" alt="Logo"></a>
						<div>
							International
						</div>
					</div>
					<div class="social_cont">
						<ul>
							<li>
								<a target="_blank" href="https://www.facebook.com">
									<span class="fa-stack fa-md" id="facebook">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://www.linkedin.com">
									<span class="fa-stack fa-md" id="linkedin">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://www.twitter.com">
									<span class="fa-stack fa-md" id="twitter">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</li>
						</ul>


					</div>
				</div>
			</div>

		</div>
	</div>

</header>
<div class="second_nav">
	<div class="container">
		<div class="row">

			<div class="col-md-10 submenu_nav">
				<ul >
					<?php 

					if (!empty(main_head_nav(2))): ?>
					<?php foreach (main_head_nav(2) as $mhn):
					if ($mhn['menu_type'] == "category") {
						$mtype = "c";
					}else{
						$mtype = "p";
					}
					?>
					<?php if ($mhn['menu_parent']==0): ?>
						
						
						<li class = "sec_nav"><a href="<?=base_url($mtype.'/'.$mhn['page_slug'])?>"><?=$mhn['page_title']?></a>
							<?php $sub_menu = get_sub_menu($mhn['menu_id']);?>
							<?php if (count($sub_menu)>=1): ?>
								
								
								<ul class="dropit">
									<?php foreach ($sub_menu as $sb ):
									if ($sb['menu_type'] == "category") {
										$sbtype = "c";
									}else{
										$sbtype = "p";
									}
									?>
									<li class = "sec_nav"><a href="<?=base_url($sbtype.'/'.$sb['page_slug'])?>"><?=$sb['page_title']?></a>
									<?php endforeach ?>

								</ul>
							<?php endif ?>
						</li>
					<?php endif ?>

				<?php endforeach ?>
			<?php endif ?>					

		</ul>

	</div>

	<div class="col-md-2 text-right search_cont">
					<!-- <ul>
						<li>
							<a target="_blank" href="">
								<span class="fa-stack fa-md" id="search">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-search fa-stack-1x fa-inverse"></i>
								</span>
							</a>
						</li>
						<li>
							<a target="_blank" href="">
								<span class="fa-stack fa-md" id="login">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-user fa-stack-1x fa-inverse"></i>
								</span>
							</a>
						</li>
					</ul> -->
				</div>  

			</div>
		</div>
	</div>
	<?php if (empty($locations)): ?>
		<div id="banner_cont" class="owl-carousel ">
			<?php if (!empty($banner)): ?>
				<?php foreach ($banner as $bn): ?>

					<div class="item ">
						<div class="blackpath"></div>
						<img src="<?php echo base_url('uploads/banner/'.$bn->banner_path)?>" alt="Banner 1">
						<div class="content-banner">
							<div class="banner-content-text">
								<div class="banner-title"><?=$bn->banner_title?></div>
								<p class="banner-description"><?=$bn->banner_description?></p>

								<!-- <a target="_blank" href="http://www.moorestephens.co.uk/pages/brexit" class="btn btn-small">Read more </a> -->
							</div>
						</div>
			<!-- <div class="owl-controls clickable">
		        <div class="owl-buttons">
		            <div class="prev">prev</div>
		            <div class="next">next</div>
		        </div>
		    </div> -->
		</div>



			<?php endforeach ?>
		<?php endif ?>
	</div>
<?php else: ?>
<?php $this->load->view('location'); ?>
	<?php endif ?>

<!-- <div class="container main-content"> -->
	<?php  $this->load->view($info['vfile']); ?>
	<!-- </div> -->

	<div class="sitemap_cont">

		<div class="container">
			<div class="row">
				<div class="sitemap">

					<div class="col-md-3">
						<ul>
							<li><a href="">Home</a></li>
							<li><a href="">About</a></li>
							<li><a href="">Join Us</a></li>
							<li><a href="">Contact</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul>
							<li><a href="">Locations</a></li>
							<li><a href="">People</a></li>
							<li><a href="">Sectors</a></li>
							<li><a href="">Services</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul>
							<li><a href="">News</a></li>
							<li><a href="">Publications</a></li>
							<li><a href="">LinkedIn</a></li>
							<li><a href="">Twitter</a></li>
							<li><a href="">Facebook</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 text-center mini_map">
					<img src="<?= base_url('inc/img/logo.png')?>" alt="Logo">
					<img src="<?= base_url('inc/img/mini-map.png')?>" alt="Logo">
					<a href="">Find a member firm</a>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">

		<div class="container">
			<div class="row">
				<div class="col-md-10 copyright_cont">
					<div>
						© Moore Stephens International Limited
					</div>
					<ul>
						<li><a href="">Privacy policy</a></li>
						<li><a href="">Legal</a></li>
						<li><a href="">Sitemap</a></li>
					</ul>
				</div>
				<div class="col-md-2">
					<div class="social_cont">
						<ul>
							<li>
								<a target="_blank" href="https://www.facebook.com">
									<span class="fa-stack fa-md" id="facebook">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://www.linkedin.com">
									<span class="fa-stack fa-md" id="linkedin">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://www.twitter.com">
									<span class="fa-stack fa-md" id="twitter">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	$(document).ready(function(){
		var owl = $('#banner_cont').owlCarousel({
			items : 1,
			lazyLoad : true,
			loop:true,
			autoplay:true,
			autoplayTimeout:3000,
			nav: true
		});
		$(".prev").click(function () {
			owl.trigger('prev.owl.carousel');
		});

		$(".next").click(function () {
			owl.trigger('next.owl.carousel');
		});
	});

	</script
	</html>