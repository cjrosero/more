<!DOCTYPE html>
<html>
  <head class="no-js">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>Moore Stephens</title>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/normalize.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/slick.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/datepicker.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css"><link rel="stylesheet" href="<?=base_url();?>assets/font/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="css/iefix.css">
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--[if lte IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <figure class="overlay"></figure>
    <header class="main-header">
      <div class="upper">
        <div class="container-fluid-width clearfix"><a href="<?=base_url()?>" class="logo"><img src="<?=base_url();?>assets/img/logo.png"></a>
          <div class="upper-nav">
            <!-- <a href="#">About</a>
            <a href="#">Contact Us</a>
            <a href="#">Join Us</a> -->
           <a href="https://www.moorestephens.com/" target="_blank">International</a> 
            <div class="social-media"><a href="#" class="fa fa-facebook"></a><a href="#" class="fa fa-linkedin"></a><a href="#" class="fa fa-twitter"></a></div>
          </div>
        </div></div>
      <div class="lower-nav main-nav">
        <div class="container-fluid-width clearfix">
          <div class="pull-left">
      <?php 

          if (!empty(main_head_nav(2))): ?>
          <?php foreach (main_head_nav(2) as $mhn):
          if ($mhn['menu_type'] == "category") {
            $mtype = "c";
          }else{
            $mtype = "p";
          }
          ?>
          <?php if ($mhn['menu_parent']==0): ?>
            <span class="nav-item">
                <a href="<?=base_url($mtype.'/'.$mhn['page_slug'])?>"><?=$mhn['page_title']?></a>
              <?php $sub_menu = get_sub_menu($mhn['menu_id']);?>
              <?php if (count($sub_menu)>=1): ?>
                  <div class="dropdown">
                  <?php foreach ($sub_menu as $sb ):
                  if ($sb['menu_type'] == "category") {
                    $sbtype = "c";
                  }else{
                    $sbtype = "p";
                  }
                  ?>
                  
              <a href="<?=base_url($sbtype.'/'.$sb['page_slug'])?>">
              <span><?=$sb['page_title']?></span>
              </a>
             
                 
                  <?php endforeach ?>
                   </div>

              
              <?php endif ?>
          
          <?php endif ?>
         </span>
        <?php endforeach ?>
      <?php endif ?>    

            <!-- <a href="#" class="nav-item active">News &amp; News</a> -->
             </div>
          <div class="pull-right"><a href="#" class="fa fa-search"></a><a href="#" class="fa fa-user-o"></a>
            <form class="search">
              <input type="text" placeholder="Search">
              <button class="fa fa-search"></button>
            </form>
          </div>
          <div class="toggle ltablet-only">
            <div class="icon"><span></span><span></span><span></span></div>
            <div class="text">Menu</div>
          </div>
        </div>
      </div>
    </header>
    <div class="main-section">
      <section class="content-section hero-slider">
        <div class="slide-container">
            <?php if (!empty($banner)): ?>
        <?php foreach ($banner as $bn): ?>
          <div class="slide slide-left hidden">
            <div class="img-container"><img src="<?php echo base_url('uploads/banner/'.$bn->banner_path)?>"></div>
            <div class="container-fluid-width">
             <!--  <div class="slide-desc">
                <h2><?=$bn->banner_title?></h2>
                <p><?=$bn->banner_description?></p>
                <a href="#" class="btn">Find out more<i class="fa fa-arrow-right"></i></a>
              </div> -->
            </div>
          </div>

      <?php endforeach ?>
    <?php endif ?>
        
        </div>
        <div class="slide-control"><span class="arrow left fa fa-caret-left"></span><span class="arrow right fa fa-caret-right"></span></div>
      </section>
<?php  $this->load->view($info['vfile']); ?>
    </div>
    <footer class="main-footer">
      <div class="footer-link">
        <div class="container-fluid-width row">
          <div class="col-3 ltab-col-4 mob-col-12">
            <h6><a href="#">Home</a></h6>
            <h6><a href="#">About</a></h6>
            <h6><a href="#">Join Us</a></h6>
            <h6><a href="#">Contact</a></h6>
          </div>
          <div class="col-3 ltab-col-4 mob-col-12">
            <h6><a href="#">Locations</a></h6>
            <h6><a href="#">People</a></h6>
            <h6><a href="#">Sectors</a></h6>
            <h6><a href="#">Services</a></h6>
          </div>
          <div class="col-3 ltab-col-4 mob-col-12">
            <h6><a href="#">News</a></h6>
            <h6><a href="#">Publications</a></h6>
            <h6><a href="#">LinkedIn</a></h6>
            <h6><a href="#">Twitter</a></h6>
            <h6><a href="#">Facebook</a></h6>
          </div>
          <div class="col-3 ltab-col-12 footer-logo"><img src="img/logo.png"><img src="img/mini-map.png"><a href="#">Find a member firm</a></div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container-fluid-width clearfix">
          <div class="pull-left"><span>&copy; Moore Stephens International</span><span>|</span><a href="#">Limited</a><span>|</span><a href="#">Privacy policy</a><span>|</span><a href="#">Legal Sitemap</a></div>
          <div class="pull-right social-media"><a href="#" class="fa fa-facebook"></a><a href="#" class="fa fa-linkedin"></a><a href="#" class="fa fa-twitter"></a></div>
        </div>
      </div>
    </footer>
    <script src="<?=base_url();?>assets/js/jquery.js"></script>
    <script src="<?=base_url();?>assets/js/slick.min.js"></script>
    <script src="<?=base_url();?>assets/js/script.js"></script>
  </body>
</html>