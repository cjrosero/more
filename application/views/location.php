       
         <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
       height: 490px;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
       <script>
      // This example uses SVG path notation to add a vector-based symbol
      // as the icon for a marker. The resulting icon is a star-shaped symbol
      // with a pale yellow fill and a thick yellow border.

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: {lat: -0.9738547, lng: 40.1505394}
        });

        

        var marker = new google.maps.Marker({
          position: map.getCenter(),
          icon: {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 10
          },
          draggable: true,
          map: map
        });

      }
    </script>
    <div id="map"></div> 

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUNdSXjUrGysVjIdUVf_4wgpYDNCaO0U8&callback=initMap">
  </script>
 


    <?php $this->load->view('lo'); ?>

    