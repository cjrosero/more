<div class="row">
<?php foreach ($people as $p): ?>
	

	<div class="loc_info col-md-5">
		<div class="people">
			<div class="body-people">
				<div class="loc_place"><a href="<?=base_url('people/'.$p->p_slug)?>"><?=$p->p_name?></a></div>
				<div class="loc_addr people-position"><?=$p->p_position?></div>
				<div class="loc_place"><?=$p->p_address?></div>
			</div>
			<div class="footer-people">
				<div class="loc_contact">Tel: <a href="tel:+962 5660709"><?=$p->p_contact?></a></div>
				<div class="loc_contact"><a class="btn btn-small" href="mailto:<?=$p->p_email?>">Email</a></div>
			</div>
		</div>
	</div>


<?php endforeach ?>
</div>